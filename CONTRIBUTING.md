# Style Guide

The Plasma Equilibrium Toolkit is written in modern C++.  The toolkit makes extensive use of the Standard Template Library, and should adhere to the C++17 standard.

1. Use C++11 threads
2. Express parallelism through task-based, shared-memory concepts, such as `std::future<T>`, `std::async<T>`, and parallel execution policies such as `std::execution::par`.
3. Use `std::unique_ptr<T>` and `std::shared_ptr<T>` instead of creating and deleting raw pointers
4. Pass by `const` reference should be the default argument for functions
5. Use CamelCase when writing variables and functions, be verbose with name
6. All function names should start with a lowercase letter
7. All class private member variables should being with `_`, and public member functions for retrieving and applying variables should be the variable name without an underscore

Here is an example class:
```c++
class ExampleClass {
  public:
    // Class constructors other than default
    ExampleClass(const int& arg1);

    // Qualify functions that retrieve variables with const
    // Inline functions when appropriate
    inline auto X() const { return _X; };

    inline void X(const double& value) { _X = value; };

  protected:
  private:
    double _X;
};
```

# Typically this makefile is only called from make in the parent directory,
# in which the variables FC and EXTRA_COMPILE_FLAGS are set.

IC_f_SRC = $(shell find Sources/Initialization_Cleanup -name '*.f' | sed "s|&\./||")
IC_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(IC_f_SRC))))
IC_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(IC_f_SRC))))
IC_f90_SRC = $(shell find Sources/Initialization_Cleanup -name '*.f90' | sed "s|&\./||")
IC_f90_OBJ = $(addprefix obj/,$(subst .f90,.o,$(notdir $(IC_f90_SRC))))
IC_f90_MOD = $(addprefix obj/,$(subst .f90,.mod,$(notdir $(IC_f90_SRC))))
H_f_SRC = $(shell find Sources/Hessian -name '*.f' | sed "s|&\./||")
H_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(H_f_SRC))))
H_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(H_f_SRC))))
G_f_SRC = $(shell find Sources/General -name '*.f' | sed "s|&\./||")
G_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(G_f_SRC))))
G_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(G_f_SRC))))
G_f90_SRC = $(shell find Sources/General -name '*.f90' | sed "s|&\./||")
G_f90_OBJ = $(addprefix obj/,$(subst .f90,.o,$(notdir $(G_f90_SRC))))
G_f90_MOD = $(addprefix obj/,$(subst .f90,.mod,$(notdir $(G_f90_SRC))))
IO_f_SRC = $(shell find Sources/Input_Output -name '*.f' | sed "s|&\./||")
IO_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(IO_f_SRC))))
IO_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(IO_f_SRC))))
NV_f_SRC = $(shell find Sources/NESTOR_vacuum -name '*.f' | sed "s|&\./||")
NV_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(NV_f_SRC))))
NV_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(NV_f_SRC))))
S_f_SRC = $(shell find Sources/Splines -name '*.f' | sed "s|&\./||")
S_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(S_f_SRC))))
S_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(S_f_SRC))))
TS_f_SRC = $(shell find Sources/TimeStep -name '*.f' | sed "s|&\./||")
TS_f_OBJ = $(addprefix obj/,$(subst .f,.o,$(notdir $(TS_f_SRC))))
TS_f_MOD = $(addprefix obj/,$(subst .f,.mod,$(notdir $(TS_f_SRC))))
TS_f90_SRC = $(shell find Sources/TimeStep -name '*.f90' | sed "s|&\./||")
TS_f90_OBJ = $(addprefix obj/,$(subst .f90,.o,$(notdir $(TS_f90_SRC))))
TS_f90_MOD = $(addprefix obj/,$(subst .f90,.mod,$(notdir $(TS_f90_SRC))))

VMEC_TARGET = obj/libvmec.a
VMEC_EXEC = xvmec2000

PRECOMP = -cpp -DLINUX -DMPI_OPT -DNETCDF
FC_COMPILE_FLAGS = -I${F95ROOT}/include/intel64/lp64 -m64 -I${MKLROOT}/include -I ${NETCDF_F_DIR}/include -I ../LIBSTELL/obj -I obj -J obj 
FLINKER = mpifort -O3 -march=native -ffree-line-length-none


.PHONY: all vmec_clean

all: $(VMEC_TARGET)
exec: $(VMEC_EXEC)

include VMEC2000.dep

%.o: %.f90
obj/%.o: Sources/General/%.f90
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f90
obj/%.o: Sources/Initialization_Cleanup/%.f90
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f90
obj/%.o: Sources/TimeStep/%.f90
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/General/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/Initialization_Cleanup/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/Hessian/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/Input_Output/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/NESTOR_vacuum/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/Splines/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

%.o: %.f
obj/%.o: Sources/TimeStep/%.f
	$(FLINKER) $(FC_COMPILE_FLAGS) $(PRECOMP) -c -o $@ $<

$(VMEC_TARGET): $(G_f_OBJ) $(G_f90_OBJ) $(IC_f_OBJ) $(IC_f90_OBJ) $(H_f_OBJ) $(IO_f_OBJ) $(NV_f_OBJ) $(S_f_OBJ) $(TS_f_OBJ) $(TS_f90_OBJ)
	ar rcs $@ $^

$(VMEC_EXEC): $(G_f_OBJ) $(G_f90_OBJ) $(IC_f_OBJ) $(IC_f90_OBJ) $(H_f_OBJ) $(IO_f_OBJ) $(NV_f_OBJ) $(S_f_OBJ) $(TS_f_OBJ) $(TS_f90_OBJ)
	$(FLINKER) $(FC_COMPILE_FLAGS) -o $@ $^ ../LIBSTELL/obj/libstell.a -L${NETCDF_C_DIR}/lib -lnetcdf -L${NETCDF_F_DIR}/lib -lnetcdff ${F95ROOT}/lib/intel64/libmkl_blas95_lp64.a ${F95ROOT}/lib/intel64/libmkl_lapack95_lp64.a -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_scalapack_lp64 -lmkl_gf_lp64 -lmkl_sequential -lmkl_core -lmkl_blacs_intelmpi_lp64 -lpthread -lm -ldl

vmec_clean:
	rm -f obj/*.o obj/*.mod obj/*.MOD $(VMEC_TARGET)

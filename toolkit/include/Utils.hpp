#if !defined(_UTILS_HPP_)
#define _UTILS_HPP_
#include <string>

namespace Utils{
  void errorHandlerNC(const int nc_status);
  void warningMsg(const std::string& warning);
}

#endif

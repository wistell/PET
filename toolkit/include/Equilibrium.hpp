#if !defined(_EQUILIBRIUM_HPP_)
#define _EQUILIBRIUM_HPP_

#include <vector>
#include <functional>

#include "boost/math/interpolators/cubic_b_spline.hpp"

#include "Definitions.hpp"

class VMEC;

/*
 * Structure - The EqObject class has the following structure:
 * EqObject -> FluxSurface
 *
 * The rationale is the EqObject will have one or more FluxSurface, and each FluxSurface
 * will have one or more Fieldline.  Thanks to operator overloading, the enclosing EqObject
 * class can infer what date is when using the retrieval/setting functions.
 */

class EqObject {
  public:
    class FluxSurface {
      public:
        using dataFuncPtr = const matReal& (FluxSurface::*)() const;
        FluxSurface(const int& nx2,const int& nx3);
        FluxSurface(FluxSurface&& refSurf);
        FluxSurface(const FluxSurface&& refSurf);
        FluxSurface(const FluxSurface& surfOld);
        void interpolateFieldline(const int& flIdx);
        struct Interpolator {
          FluxSurface* _surface;
          int _nRows, _nCols;
          vecReal _rowVals;
          Interpolator(FluxSurface* fs);
          real operator()(dataFuncPtr ptr,const real& x2, const real& x3);
        };

        // Inline functions for retrieving values as a function of indices
        inline real g11(const int& idx2, const int& idx3) const { return _g11(idx2,idx3); };
        inline real g12(const int& idx2, const int& idx3) const { return _g12(idx2,idx3); };
        inline real g22(const int& idx2, const int& idx3) const { return _g22(idx2,idx3); };
        inline real g13(const int& idx2, const int& idx3) const { return _g13(idx2,idx3); };
        inline real g23(const int& idx2, const int& idx3) const { return _g23(idx2,idx3); };
        inline real g33(const int& idx2, const int& idx3) const { return _g33(idx2,idx3); };
        inline real dBdx1(const int& idx2, const int& idx3) const { return _dBdx1(idx2,idx3); };
        inline real dBdx2(const int& idx2, const int& idx3) const { return _dBdx2(idx2,idx3); };
        inline real dBdx3(const int& idx2, const int& idx3) const { return _dBdx3(idx2,idx3); };
        inline real Bmag(const int& idx2, const int& idx3) const { return _Bmag(idx2,idx3); };
        inline real jac(const int& idx2, const int& idx3) const { return _jac(idx2,idx3); };

        inline real cylR(const int& idx2, const int& idx3) const { return _cylR(idx2,idx3); };
        inline real cylZ(const int& idx2, const int& idx3) const { return _cylZ(idx2,idx3); };
        inline real cylTheta(const int& idx2, const int& idx3) const { return _cylTheta(idx2,idx3); };

        inline real X(const int& idx2, const int& idx3) const { return _X(idx2,idx3); };
        inline real Y(const int& idx2, const int& idx3) const { return _Y(idx2,idx3); };
        inline real Z(const int& idx2, const int& idx3) const { return _Z(idx2,idx3); };

        inline intTuple2D nx() const { return _nx; };
        inline real iota() const { return _iota; };
        inline real shat() const { return _shat; };
        inline real x1() const { return _x1; };

        inline int nfp() const { return _nfp; };

        inline real x2(const int& idx2) const { return _x2[idx2]; };
        inline real x3(const int& idx3) const { return _x3[idx3]; };

        // Inline functions for retrieving a const reference to data vectors/arrays
        inline const vecReal& x2() const { return _x2; };
        inline const vecReal& x3() const { return _x3; };
        
        inline const matReal& Bmag() const { return _Bmag; };
        inline const matReal& jac() const { return _jac; };
        inline const matReal& g11() const { return _g11; };
        inline const matReal& g12() const { return _g12; };
        inline const matReal& g22() const { return _g22; };
        inline const matReal& g13() const { return _g13; };
        inline const matReal& g23() const { return _g23; };
        inline const matReal& g33() const { return _g33; };
        inline const matReal& dBdx1() const { return _dBdx1; };
        inline const matReal& dBdx2() const { return _dBdx2; };
        inline const matReal& dBdx3() const { return _dBdx3; };

        // Inline functions for setting values
        inline void g11(const int& idx2, const int& idx3, const real& value) { _g11(idx2,idx3) = value; };
        inline void g12(const int& idx2, const int& idx3, const real& value) { _g12(idx2,idx3) = value; };
        inline void g22(const int& idx2, const int& idx3, const real& value) { _g22(idx2,idx3) = value; };
        inline void g13(const int& idx2, const int& idx3, const real& value) { _g13(idx2,idx3) = value; };
        inline void g23(const int& idx2, const int& idx3, const real& value) { _g23(idx2,idx3) = value; };
        inline void g33(const int& idx2, const int& idx3, const real& value) { _g33(idx2,idx3) = value; };
        inline void dBdx1(const int& idx2, const int& idx3, const real& value) { _dBdx1(idx2,idx3) = value; };
        inline void dBdx2(const int& idx2, const int& idx3, const real& value) { _dBdx2(idx2,idx3) = value; };
        inline void dBdx3(const int& idx2, const int& idx3, const real& value) { _dBdx3(idx2,idx3) = value; };
        inline void Bmag(const int& idx2, const int& idx3, const real& value) { _Bmag(idx2,idx3) = value; };
        inline void jac(const int& idx2, const int& idx3, const real& value) { _jac(idx2,idx3) = value; };

        inline void cylR(const int& idx2, const int& idx3, const real& value) { _cylR(idx2,idx3) = value; };
        inline void cylZ(const int& idx2, const int& idx3, const real& value) { _cylZ(idx2,idx3) = value; };
        inline void cylTheta(const int& idx2, const int& idx3, const real& value) { _cylTheta(idx2,idx3) = value; };

        inline void X(const int& idx2, const int& idx3, const real& value) { _X(idx2,idx3) = value; };
        inline void Y(const int& idx2, const int& idx3, const real& value) { _Y(idx2,idx3) = value; };
        inline void Z(const int& idx2, const int& idx3, const real& value) { _Z(idx2,idx3) = value; };

        inline void iota(const real& value) { _iota = value; };
        inline void shat(const real& value) { _shat = value; };
        inline void x1(const real& value) { _x1 = value; };

        inline void nfp(const int& value) { _nfp = value; };

        inline void x2(const int& idx2, const real& value) { _x2[idx2] = value; };
        inline void x3(const int& idx3, const real& value) { _x3[idx3] = value; };
      protected:
      private:
        intTuple2D _nx;

        //std::vector<Fieldline> _flines;

        real _iota; //Rotational transform
        real _shat; //Global magnetic shear
        real _x1; //Normalized toroidal flux

        int _nfp; //Number of field periods

        vecReal _x2; //Vector containing the x^2 (alpha) coordinate
        vecReal _x3; //Vector containing the x^3 (theta, zeta, distance) coordinate

        matReal _g11;
        matReal _g12;
        matReal _g22;
        matReal _g13;
        matReal _g23;
        matReal _g33;
        matReal _dBdx1;
        matReal _dBdx2;
        matReal _dBdx3;
        matReal _jac;
        matReal _Bmag;

        matReal _cylR;
        matReal _cylZ;
        matReal _cylTheta;

        matReal _X;
        matReal _Y;
        matReal _Z;

    };

    EqObject(const int& nx1, const int& nx2, const int& nx3);
    EqObject(const vecReal& x1, const int& nx2, const int& nx3);
    EqObject(const std::shared_ptr<VMEC>& vmec, const vecReal& x1, const int& nx2, const int& nzfp, const real& x3Max);
    EqObject(const std::shared_ptr<EqObject>& eqOld);
    EqObject& operator=(const EqObject& oldEq);

    inline void interpolateFieldline(const int& surfIdx, const int& flIdx) { _surfaces.at(surfIdx).interpolateFieldline(flIdx); };

    // Inline functions for retrieving values
    inline real g11(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).g11(idx2,idx3); };
    inline real g12(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).g12(idx2,idx3); };
    inline real g22(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).g22(idx2,idx3); };
    inline real g13(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).g13(idx2,idx3); };
    inline real g23(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).g23(idx2,idx3); };
    inline real g33(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).g33(idx2,idx3); };
    inline real dBdx1(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).dBdx1(idx2,idx3); };
    inline real dBdx2(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).dBdx2(idx2,idx3); };
    inline real dBdx3(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).dBdx3(idx2,idx3); };
    inline real Bmag(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).Bmag(idx2,idx3); };
    inline real jac(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).jac(idx2,idx3); };

    inline real cylR(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).cylR(idx2,idx3); };
    inline real cylZ(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).cylZ(idx2,idx3); };
    inline real cylTheta(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).cylTheta(idx2,idx3); };

    inline real X(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).X(idx2,idx3); };
    inline real Y(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).Y(idx2,idx3); };
    inline real Z(const int& idx1, const int& idx2, const int& idx3) const { return _surfaces.at(idx1).Z(idx2,idx3); };

    inline real iota(const int& idx1) { return _surfaces.at(idx1).iota(); };
    inline real shat(const int& idx1) { return _surfaces.at(idx1).iota(); };
    inline real x1(const int& idx1) { return _surfaces.at(idx1).x1(); };
    inline intTuple2D nx(const int& idx1) { return _surfaces.at(idx1).nx(); };

    inline real x2(const int& idx1, const int& idx2) { return _surfaces.at(idx1).x2(idx2); };
    inline real x3(const int& idx1, const int& idx3) { return _surfaces.at(idx1).x3(idx3); };

    inline int nfp() const { return _nfp; };
    inline int nzfp() const { return _nzfp; };
    inline real Bref() const { return _Bref; };
    inline real Lref() const { return _Lref; };
    inline real edgeFlux2Pi() const { return _edgeFlux2Pi; };
    inline int nx1() const { return _nx1; };
    inline intTuple2D nx(const int& idx1) const { return _surfaces.at(idx1).nx(); };
    inline std::string coordinates() const { return _coordinates; };
    inline std::string normLength() const { return _normLength; };
    inline std::string x3coord() const { return _x3coord; };

    inline const vecReal& x2(const int& idx1) const { return _surfaces.at(idx1).x2(); };
    inline const vecReal& x3(const int& idx1) const { return _surfaces.at(idx1).x3(); };

    // Inline functions for setting values
    inline void g11(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).g11(idx2,idx3,value); };
    inline void g12(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).g12(idx2,idx3,value); };
    inline void g22(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).g22(idx2,idx3,value); };
    inline void g13(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).g13(idx2,idx3,value); };
    inline void g23(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).g23(idx2,idx3,value); };
    inline void g33(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).g33(idx2,idx3,value); };
    inline void dBdx1(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).dBdx1(idx2,idx3,value); };
    inline void dBdx2(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).dBdx2(idx2,idx3,value); };
    inline void dBdx3(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).dBdx3(idx2,idx3,value); };
    inline void Bmag(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).Bmag(idx2,idx3,value); };
    inline void jac(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).jac(idx2,idx3,value); };

    inline void cylR(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).cylR(idx2,idx3,value); };
    inline void cylZ(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).cylZ(idx2,idx3,value); };
    inline void cylTheta(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).cylTheta(idx2,idx3,value); };

    inline void X(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).X(idx2,idx3,value); };
    inline void Y(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).Y(idx2,idx3,value); };
    inline void Z(const int& idx1, const int& idx2, const int& idx3, const real& value) { _surfaces.at(idx1).Z(idx2,idx3,value); };

    inline void iota(const int& idx1, const real& value) { _surfaces.at(idx1).iota(value); };
    inline void shat(const int& idx1, const real& value) { _surfaces.at(idx1).shat(value); };
    inline void x1(const int& idx1, const real& value) { _surfaces.at(idx1).x1(value); }; 

    inline void nfp(const int& value) { _nfp = value; for(auto& surf : _surfaces) { surf.nfp(value); } }; 
    inline void nzfp(const int& value) { _nzfp = value; };
    inline void Bref(const real& value) { _Bref = value; };
    inline void Lref(const real& value) { _Lref = value; };
    inline void coordinates(const std::string& value) { _coordinates = value; };
    inline void normLength(const std::string& value) { _normLength = value; };
    inline void x3coord(const std::string& value) { _x3coord = value; };

    inline void x2(const int& idx1, const int& idx2, const real& value) { _surfaces.at(idx1).x2(idx2,value); };
    inline void x3(const int& idx1, const int& idx3, const real& value) { _surfaces.at(idx1).x3(idx3,value); };

    inline const FluxSurface& getFluxSurface(const int& idx1) const { return _surfaces.at(idx1); };
  protected:
  private:
    // String for the coordinate type
    // Current options: "pest", "boozer"
    std::string _coordinates;
    std::string _normLength;
    std::string _x3coord;

    // Integer tuple containing the dimensions of the equilibrium
    int _nx1;

    // Number of field periods
    int _nfp;
    // Number of points along field lines per field period
    int _nzfp;

    // Dimensionful reference values
    real _Bref;
    real _Lref;
    real _edgeFlux2Pi;

    vecInt _sIdx; //Vector containing the integer index of the surface

    std::vector<FluxSurface> _surfaces;
};
#endif

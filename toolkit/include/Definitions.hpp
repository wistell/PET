#if !defined(_DEFINITIONS_HPP_)
#define _DEFINITIONS_HPP_
#include <complex>
#include <tuple>
#include <string>
#include <cmath>

#include "blaze/math/DynamicMatrix.h"
#include "blaze/math/DynamicVector.h"

#if defined(DEBUG)
#define DebugMsg(x) do { x } while(0)
#else
#define DebugMsg(x) do { } while(0)
#endif

using c_int = std::complex<int>;
#if defined(SINGLE_PREC)
using real = float;
using cplx = std::complex<float>;
#else
using real = double;
using cplx = std::complex<double>;
#endif

constexpr real pi = 3.1415926535897932846264338327950;
constexpr real mu0 = 4*pi*1e-7;

// Define default vectors with default allocator
using vecInt = blaze::DynamicVector<int>;
using vecReal = blaze::DynamicVector<real>;
using vecCplx = blaze::DynamicVector<cplx>;

using rowVecInt = blaze::DynamicVector<int,blaze::rowVector>;
using colVecInt = blaze::DynamicVector<int,blaze::columnVector>;
using rowVecLInt = blaze::DynamicVector<long int,blaze::rowVector>;
using colVecLInt = blaze::DynamicVector<long int,blaze::columnVector>;
using rowVecReal = blaze::DynamicVector<real,blaze::rowVector>; using colVecReal = blaze::DynamicVector<real,blaze::columnVector>;
using rowVecCplx = blaze::DynamicVector<cplx,blaze::rowVector>;
using colVecCplx = blaze::DynamicVector<cplx,blaze::columnVector>;

// Define default matrix with default allocator
using matInt = blaze::DynamicMatrix<int>;
using matReal = blaze::DynamicMatrix<real>;
using matCplx = blaze::DynamicMatrix<cplx>;

using rowMatInt = blaze::DynamicMatrix<int,blaze::rowMajor>;
using colMatInt = blaze::DynamicMatrix<int,blaze::columnMajor>;
using rowVMatInt = blaze::DynamicMatrix<long int,blaze::rowMajor>;
using colMatLInt = blaze::DynamicMatrix<long int,blaze::columnMajor>;
using rowMatReal = blaze::DynamicMatrix<real,blaze::rowMajor>;
using colMatReal = blaze::DynamicMatrix<real,blaze::columnMajor>;
using rowMatCplx = blaze::DynamicMatrix<cplx,blaze::rowMajor>;
using colMatCplx = blaze::DynamicMatrix<cplx,blaze::columnMajor>;

using intTuple2D = std::tuple<int,int>;
using intTuple3D = std::tuple<int,int,int>;

#endif

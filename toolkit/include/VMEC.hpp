#if !defined(_VMEC_HPP_)
#define _VMEC_HPP_

#include <memory>
#include <string>
#include <iterator>
#include <utility>

#include "netcdf"

#include "Definitions.hpp"

class VMEC {
  public:
    // Default constructor, destructor sufficient as all contained objects have constructor/destructor 

    /**
     * void readNetCDF4(const string filename)
     * Reads a VMEC file in NetCDF4 format
     */
    void readNetCDF4(const std::string& filename);

    /**
     * void populateVar(const std::iterator<std::pair<std::string,netCDF::NcVar>> varIter)
     * Takes a variable from a NetCDF4 file and loads the corresponding local variable
     * netCDF::NcFile.getVars() returns a collection of std::pair<std::string,netCDF::NcVar>
     */
    void populateVar(const std::pair<std::string,netCDF::NcVar>& varIter);

    /**
     * void checkVMEC()
     * Checks the loaded VMEC variables for physical consistency
     */
    void checkVMEC();

    /**
     * std::pair<real,real> computeIotaShatPair(const real& s)
     * Computes a pair containing the rotational transform
     * and average magnetic shear at flux surface s
     */
    std::pair<real,real> computeIotaShatPair(const real& s);

    // Public inline functions for retrieving data VMEC data
    inline int nSurf() const { return _nSurf; };
    inline int mPol() const { return _mPol; };
    inline int nTor() const { return _nTor; };

    inline bool lasym() const { return _lasym; };

    inline int signgs() const { return _signgs; };

    inline int nfp() const { return _nfp; };

    inline int mnmax() const { return _mnmax; };
    inline int mnmax_nyq() const { return _mnmax_nyq; };

    inline real AMinor() const { return _AMinor; };
    inline real RMajor() const { return _RMajor; };
    inline real BEdge() const { return _BEdge; };


    inline real betaTotal() const { return _betaTotal; };
    inline real betaTor() const { return _betaTor; };
    inline real betaPol() const { return _betaPol; };
    inline real betaAxis() const { return _betaAxis; };

    // Public inline functions for retrieving vector and array data as a function of indices
    inline real phi(const int surfIdx) const { return _phi[surfIdx]; };
    inline real phipFull(const int surfIdx) const { return _phipFull[surfIdx]; };
    inline real phipHalf(const int surfIdx) const { return _phipHalf[surfIdx]; };
    inline real iotaFull(const int surfIdx) const { return _iotaFull[surfIdx]; };
    inline real iotaHalf(const int surfIdx) const { return _iotaHalf[surfIdx]; };
    inline real presFull(const int surfIdx) const { return _presFull[surfIdx]; };
    inline real presHalf(const int surfIdx) const { return _presHalf[surfIdx]; };
    inline real betaVol(const int surfIdx) const { return _betaVol[surfIdx]; };

    inline real xm(const int mnIdx) const { return _xm[mnIdx]; };
    inline real xn(const int mnIdx) const { return _xn[mnIdx]; };
    inline real xmnyq(const int mnIdx) const { return _xmnyq[mnIdx]; };
    inline real xnnyq(const int mnIdx) const { return _xnnyq[mnIdx]; };

    inline real rmnc(const int surfIdx, const int mnIdx) const { return _rmnc(surfIdx,mnIdx); };
    inline real rmns(const int surfIdx, const int mnIdx) const { return _rmns(surfIdx,mnIdx); };
    inline real zmnc(const int surfIdx, const int mnIdx) const { return _zmnc(surfIdx,mnIdx); };
    inline real zmns(const int surfIdx, const int mnIdx) const { return _zmns(surfIdx,mnIdx); };
    inline real bmnc(const int surfIdx, const int mnIdx) const { return _bmnc(surfIdx,mnIdx); };
    inline real bmns(const int surfIdx, const int mnIdx) const { return _bmns(surfIdx,mnIdx); };
    inline real gmnc(const int surfIdx, const int mnIdx) const { return _gmnc(surfIdx,mnIdx); };
    inline real gmns(const int surfIdx, const int mnIdx) const { return _gmns(surfIdx,mnIdx); };
    inline real lmnc(const int surfIdx, const int mnIdx) const { return _lmnc(surfIdx,mnIdx); };
    inline real lmns(const int surfIdx, const int mnIdx) const { return _lmns(surfIdx,mnIdx); };
    inline real bsubsmnc(const int surfIdx, const int mnIdx) const { return _bsubsmnc(surfIdx,mnIdx); };
    inline real bsubsmns(const int surfIdx, const int mnIdx) const { return _bsubsmns(surfIdx,mnIdx); };
    inline real bsubumnc(const int surfIdx, const int mnIdx) const { return _bsubumnc(surfIdx,mnIdx); };
    inline real bsubumns(const int surfIdx, const int mnIdx) const { return _bsubumns(surfIdx,mnIdx); };
    inline real bsubvmnc(const int surfIdx, const int mnIdx) const { return _bsubvmnc(surfIdx,mnIdx); };
    inline real bsubvmns(const int surfIdx, const int mnIdx) const { return _bsubvmns(surfIdx,mnIdx); };
    inline real bsupumnc(const int surfIdx, const int mnIdx) const { return _bsupumnc(surfIdx,mnIdx); };
    inline real bsupumns(const int surfIdx, const int mnIdx) const { return _bsupumns(surfIdx,mnIdx); };
    inline real bsupvmnc(const int surfIdx, const int mnIdx) const { return _bsupvmnc(surfIdx,mnIdx); };
    inline real bsupvmns(const int surfIdx, const int mnIdx) const { return _bsupvmns(surfIdx,mnIdx); };

    // Public inline functions for returning const references to the data structures
    inline const vecReal& phi() const { return _phi; };
    inline const vecReal& iotaFull() const { return _iotaFull; };
    inline const vecReal& iotaHalf() const { return _iotaHalf; };
    inline const vecReal& presFull() const { return _presFull; };
    inline const vecReal& presHalf() const { return _presHalf; };
    inline const vecInt& xm() const { return _xm; };
    inline const vecInt& xn() const { return _xn; };
    inline const vecInt& xmnyq() const { return _xmnyq; };
    inline const vecInt& xnnyq() const { return _xnnyq; };

    inline const colMatReal& bmnc() const { return _bmnc; }; 
    inline const colMatReal& bmns() const { return _bmns; }; 
    inline const colMatReal& rmnc() const { return _rmnc; }; 
    inline const colMatReal& rmns() const { return _rmns; }; 
    inline const colMatReal& zmnc() const { return _zmnc; }; 
    inline const colMatReal& zmns() const { return _zmns; }; 
    inline const colMatReal& lmnc() const { return _lmnc; }; 
    inline const colMatReal& lmns() const { return _lmns; }; 
    inline const colMatReal& gmnc() const { return _gmnc; }; 
    inline const colMatReal& gmns() const { return _gmns; }; 
    inline const colMatReal& bsubsmnc() const { return _bsubsmnc; }; 
    inline const colMatReal& bsubsmns() const { return _bsubsmns; }; 
    inline const colMatReal& bsubumnc() const { return _bsubumnc; }; 
    inline const colMatReal& bsubumns() const { return _bsubumns; }; 
    inline const colMatReal& bsubvmnc() const { return _bsubvmnc; }; 
    inline const colMatReal& bsubvmns() const { return _bsubvmns; }; 
    inline const colMatReal& bsupumnc() const { return _bsupumnc; }; 
    inline const colMatReal& bsupumns() const { return _bsupumns; }; 
    inline const colMatReal& bsupvmnc() const { return _bsupvmnc; }; 
    inline const colMatReal& bsupvmns() const { return _bsupvmns; }; 
    inline const colMatReal& currumnc() const { return _currumnc; }; 
    inline const colMatReal& currumns() const { return _currumns; }; 
    inline const colMatReal& currvmnc() const { return _currvmnc; }; 
    inline const colMatReal& currvmns() const { return _currvmns; }; 

  protected:
    
  private:
    std::string _filename;

    // VMEC dimensions
    int _nSurf;
    int _mPol;
    int _nTor;
    // Asymmetric flag
    bool _lasym;
    // Sign of toroidal flux
    int _signgs;

    // Number of field periods
    int _nfp;

    // Maximum MN number for non-Nyquist
    int _mnmax;
    // Maximum MN number for Nyquist
    int _mnmax_nyq;

    // Dimensionful quantities for determining normalizations
    real _AMinor;
    real _RMajor;
    real _BEdge;

    // Beta values
    real _betaTotal;
    real _betaPol;
    real _betaTor;
    real _betaAxis;
    
    // Coefficient matrices
    // These will have dimension ns x n_modes
    colMatReal _rmnc;
    colMatReal _rmns;
    colMatReal _zmnc;
    colMatReal _zmns;
    colMatReal _bmnc;
    colMatReal _bmns;
    colMatReal _gmnc;
    colMatReal _gmns;
    colMatReal _lmnc;
    colMatReal _lmns;
    colMatReal _bsubsmnc;
    colMatReal _bsubsmns;
    colMatReal _bsubumnc;
    colMatReal _bsubumns;
    colMatReal _bsubvmnc;
    colMatReal _bsubvmns;
    colMatReal _bsupumnc;
    colMatReal _bsupumns;
    colMatReal _bsupvmnc;
    colMatReal _bsupvmns;
    colMatReal _currvmnc;
    colMatReal _currvmns;
    colMatReal _currumnc;
    colMatReal _currumns;

    // Coefficient vectors
    vecInt _xm;
    vecInt _xn;
    vecInt _xmnyq;
    vecInt _xnnyq;
    vecReal _phi;
    vecReal _phipFull;
    vecReal _phipHalf;
    vecReal _iotaFull;
    vecReal _iotaHalf;
    vecReal _presFull;
    vecReal _presHalf;
    vecReal _betaVol;
};

#endif

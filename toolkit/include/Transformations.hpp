#if !defined(_TRANSFORMATIONS_HPP_)
#define _TRANSFORMATIONS_HPP_
#include <memory>

#include "Definitions.hpp"

class VMEC;
class EqObject;

namespace Transforms {
  namespace PEST {
    void vmec2pest(const std::shared_ptr<VMEC>& vmec, std::shared_ptr<EqObject>& equilibrium);

    /**
     * real findVMECTheta(const std::shared_ptr<VMEC>& vmec, const real& guess,
     * const real& boundLower, const real& boundUpper)
     *
     * Computes the root 
     */
    real findVMECTheta(const std::shared_ptr<VMEC>& vmec, const real& surf, const real& thetaTarget, const real& interval, const real& zeta);
    EqObject geneNormalizations(const std::shared_ptr<EqObject>& eqOld);
  }
}

#endif

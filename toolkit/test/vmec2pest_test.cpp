#include <iostream>

#include "Definitions.hpp"
#include "Transformations.hpp"
#include "VMEC.hpp"
#include "Equilibrium.hpp"

int main(int argc, char* argv[]) {
  std::string filename("/home/bfaber/data/equilibria/ATEN/wout_aten.nc");
  std::shared_ptr<VMEC> vmec = std::make_shared<VMEC>();

  vmec->readNetCDF4(filename);
  vmec->checkVMEC();

  vecReal s(1,0.5);
  std::shared_ptr<EqObject> eq = std::make_shared<EqObject>(vmec,s,1,64,pi);

  eq->x3coord("zeta");
  Transforms::PEST::vmec2pest(vmec,eq);
  EqObject normalized = Transforms::PEST::geneNormalizations(eq);

}

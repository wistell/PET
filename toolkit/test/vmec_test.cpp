#include <string>
#include <iostream>
#include <iterator>
#include <memory>

#include "VMEC.hpp"
#include "Transformations.hpp"

int main(int argc, char* argv[]) {
  std::string filename("/home/bfaber/data/equilibria/ATEN/wout_aten.nc");
  std::shared_ptr<VMEC> vmec = std::make_shared<VMEC>();

  vmec->readNetCDF4(filename);
  vmec->checkVMEC();

  auto theta_vmec = Transforms::PEST::findVMECTheta(vmec, 0.632, 9.83, 0.5, 0.0);
  std::cout << "theta_vmec = " << theta_vmec << '\n';

  return 0;
}

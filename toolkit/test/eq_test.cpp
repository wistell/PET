#include <memory>
#include <iostream>

#include "Equilibrium.hpp"

int main(int argc, char* argv[]) {
  std::shared_ptr<EqObject> equilibrium = std::make_shared<EqObject>(51,32,32);
  equilibrium->g11(0,0,0,0.5); 
  auto result = equilibrium->g11(0,0,0);
  std::cout << result << '\n';
}

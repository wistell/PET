#include "Utils.hpp"
#include "Equilibrium.hpp"
#include "VMEC.hpp"

EqObject::EqObject(const int& nx1, const int& nx2, const int& nx3) {
  _nx1 = nx1;
  std::iota(_sIdx.begin(),_sIdx.end(),0);
  _surfaces = std::vector<EqObject::FluxSurface>(nx1,FluxSurface(nx2,nx3));
}

EqObject::EqObject(const vecReal& x1, const int& nx2, const int& nx3) {
  _nx1 = x1.size();
  std::iota(_sIdx.begin(),_sIdx.end(),0);
  _surfaces = std::vector<EqObject::FluxSurface>(x1.size(),FluxSurface(nx2,nx3));

  for (auto surfIdx=0; surfIdx<x1.size(); ++surfIdx) _surfaces.at(surfIdx).x1(x1[surfIdx]);
}

/*
 * Constructor for EqObject object from a VMEC object
 */
EqObject::EqObject(const std::shared_ptr<VMEC>& vmec, const vecReal& x1, const int& nx2, const int& nzfp, const real& x3Max) {
  _nfp = vmec->nfp();
  _nzfp = nzfp;
  _nx1 = x1.size();
  _edgeFlux2Pi = vmec->phi(vmec->nSurf()-1)/(2*pi)*vmec->signgs();

  if (_normLength == "minor") {
    _Lref = vmec->AMinor();
  } else if (_normLength == "major") {
    _Lref = vmec->RMajor();
  } else {
    Utils::warningMsg("Using minor radius for length normalization!");
    _Lref = vmec->AMinor();
  }

  _Bref = 2.0*std::abs(_edgeFlux2Pi)/std::pow(_Lref,2.0);

  // Compute flux surface quantities on each surface
  // Compute quantities with boost::math::cubic_b_spline interpolators 
  const real surfaceSpacing = 1.0/(vmec->nSurf()-1);
  const real sAxis = 0.0;
  const real sHalfAxis = 0.5*surfaceSpacing;

  // Const references won't be dangling after function completion, as they continue to live in the VMEC object
  const vecReal& phiFullGrid = vmec->phi();
  const vecReal& iotaHalfGrid = vmec->iotaHalf();
  const vecReal& pressureFullGrid = vmec->presFull();

  // Define the spline interpolators for iota and the pressure
  // Derivatives with respect to the normalized toroidal flux variable at location s are computed with (for example)
  // d_iota_d_s = iotaSpline.prime(s);
  boost::math::cubic_b_spline iotaSpline(iotaHalfGrid.begin(), iotaHalfGrid.end(), sAxis, surfaceSpacing);
  boost::math::cubic_b_spline pressureSpline(pressureFullGrid.begin(), pressureFullGrid.end(), sAxis, surfaceSpacing);

  for (auto surfIdx = 0; surfIdx < _nx1; ++surfIdx) { // Begin surface loop
    auto surf = x1[surfIdx]; 
    if (surf > 1.0-0.5*surfaceSpacing) throw std::runtime_error("Normalized flux surface value cannot be greater than last half mesh point!");
    if (surf < 0.5*surfaceSpacing) throw std::runtime_error("Normalized flux surface value cannot be less than the first half mesh point!");


    int nx3 = 2*(int)std::ceil(x3Max*(_nfp*_nzfp)/pi)+1;
    // Determine maximum angle in field line following coordinate
    // over which to follow the field line
    _surfaces.emplace_back(FluxSurface(nx2,nx3));

    auto iotaPair = vmec->computeIotaShatPair(surf);
    this->iota(surfIdx,iotaPair.first);
    this->shat(surfIdx,iotaPair.second); 
  }
}

EqObject& EqObject::operator=(const EqObject& eqOld) {
  if (this != &eqOld) {
    _coordinates = eqOld.coordinates();
    _normLength = eqOld.normLength();
    _x3coord = eqOld.x3coord();
    _nx1 = eqOld.nx1();
    _nzfp = eqOld.nzfp();
    _Bref = eqOld.Bref();
    _Lref = eqOld.Lref();
    _edgeFlux2Pi = eqOld.edgeFlux2Pi();
    for (auto surfIdx = 0; surfIdx < _nx1; ++surfIdx) _surfaces.emplace_back(FluxSurface(eqOld.getFluxSurface(surfIdx)));
  }
  return *this;
}

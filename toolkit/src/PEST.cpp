#include <memory>
#include <utility> 

#include "boost/math/tools/roots.hpp"
#include "boost/math/interpolators/cubic_b_spline.hpp"

#include "Utils.hpp"
#include "Transformations.hpp"
#include "VMEC.hpp"
#include "Equilibrium.hpp"


void Transforms::PEST::vmec2pest(const std::shared_ptr<VMEC>& vmec, std::shared_ptr<EqObject>& equilibrium) {
  const real surfaceSpacing = 1.0/(vmec->nSurf()-1);
  const real sAxis = 0.0;
  const real sHalfAxis = 0.5*surfaceSpacing;

  // Const references won't be dangling after function completion, as they continue to live in the VMEC object
  const vecReal& phiFullGrid = vmec->phi();
  const vecReal& iotaHalfGrid = vmec->iotaHalf();
  const vecReal& pressureFullGrid = vmec->presFull();
  const vecInt& xmnyq = vmec->xmnyq();
  const vecInt& xnnyq = vmec->xnnyq();
  const vecInt& xm = vmec->xm();
  const vecInt& xn = vmec->xn();

  // Define the spline interpolators for iota and the pressure
  // Derivatives with respect to the normalized toroidal flux variable at location s are computed with (for example)
  // d_iota_d_s = iotaSpline.prime(s);
  boost::math::cubic_b_spline iotaSpline(iotaHalfGrid.begin(), iotaHalfGrid.end(), sAxis, surfaceSpacing);
  boost::math::cubic_b_spline pressureSpline(pressureFullGrid.begin(), pressureFullGrid.end(), sAxis, surfaceSpacing);

  // For the PEST calculation, we can equate alpha = x2 and zeta = x3
  const int nSurfaces = equilibrium->nx1();

  // Retrieve surface independent quantities from the equilibrium
  const int nfp = equilibrium->nfp();
  const real Lref = equilibrium->Lref();
  const real edgeFlux2Pi = equilibrium->edgeFlux2Pi();;
  const int fluxSign = ((real)0 < edgeFlux2Pi) - (edgeFlux2Pi < real(0));
  const real Bref = equilibrium->Bref();

  // Scheme of the calculation:
  // The VMEC equilibrium is defined on the (s,theta_vmec,zeta) grid, with some quantities living
  // on the radial full mesh (at the surfaces) and at the radial half mesh (between the surfaces)
  // For a quantity A, the value of A(s,theta_vmec,zeta) can be computed by the modified
  // inverse Fourier transform in the (theta_vmec,zeta) coordinates.  
  // The PEST value can be computed by using theta_pest = theta_vmec - Lambda(theta_vmec,zeta), which require a 
  // 1D root solve for the value of theta_vmec for a given theta_pest.  The PEST coordinate alpha is given
  // by alpha = theta_pest - iota(s)*zeta.  As iota is a surface dependent quantity, the transformation must
  // be called on each surface individually
  for (auto surfIdx = 0; surfIdx < nSurfaces; ++surfIdx) { // Begin global surface loop
    auto surf = equilibrium->x1(surfIdx);
    const intTuple2D pestDims = equilibrium->nx(surfIdx);
    const int nAlpha = std::get<0>(pestDims);
    const int nZeta = std::get<1>(pestDims);

    // In the following variable declarations, 'tv' = theta_vmec and 'z' = zeta
    matReal B(nAlpha,nZeta,0.0), dBds(nAlpha,nZeta,0.0), dBdtv(nAlpha,nZeta,0.0), dBdz(nAlpha,nZeta,0.0), jacobian(nAlpha,nZeta,0.0),
          Bsuptv(nAlpha,nZeta,0.0), Bsupz(nAlpha,nZeta,0.0), Bsubtv(nAlpha,nZeta,0.0), Bsubz(nAlpha,nZeta,0.0), Bsubs(nAlpha,nZeta,0.0),
          R(nAlpha,nZeta,0.0), dRdtv(nAlpha,nZeta,0.0), dRdz(nAlpha,nZeta,0.0), Z(nAlpha,nZeta,0.0), dZdtv(nAlpha,nZeta,0.0),
          dZdz(nAlpha,nZeta,0.0), Lambda(nAlpha,nZeta,0.0), dLdtv(nAlpha,nZeta,0.0), dLdz(nAlpha,nZeta,0.0), dRds(nAlpha,nZeta,0.0),
          dZds(nAlpha,nZeta,0.0), dLds(nAlpha,nZeta,0.0);

    matReal dXds(nAlpha,nZeta,0.0), dXdtv(nAlpha,nZeta,0.0), dXdz(nAlpha,nZeta,0.0), dYds(nAlpha,nZeta,0.0), dYdtv(nAlpha,nZeta,0.0), dYdz(nAlpha,nZeta,0.0),
          grad_s_X(nAlpha,nZeta,0.0), grad_s_Y(nAlpha,nZeta,0.0), grad_s_Z(nAlpha,nZeta,0.0), grad_thetaVMEC_X(nAlpha,nZeta,0.0), 
          grad_thetaVMEC_Y(nAlpha,nZeta,0.0), grad_thetaVMEC_Z(nAlpha,nZeta,0.0), grad_zeta_X(nAlpha,nZeta,0.0),
          grad_zeta_Y(nAlpha,nZeta,0.0), grad_zeta_Z(nAlpha,nZeta,0.0), grad_theta_X(nAlpha,nZeta,0.0), 
          grad_theta_Y(nAlpha,nZeta,0.0), grad_theta_Z(nAlpha,nZeta,0.0);

    matReal grad_psi_X(nAlpha,nZeta,0.0), grad_psi_Y(nAlpha,nZeta,0.0), grad_psi_Z(nAlpha,nZeta,0.0), grad_alpha_X(nAlpha,nZeta,0.0), 
          grad_alpha_Y(nAlpha,nZeta,0.0), grad_alpha_Z(nAlpha,nZeta,0.0), grad_B_X(nAlpha,nZeta,0.0), grad_B_Y(nAlpha,nZeta,0.0), 
          grad_B_Z(nAlpha,nZeta,0.0), B_X(nAlpha,nZeta,0.0), B_Y(nAlpha,nZeta,0.0), B_Z(nAlpha,nZeta,0.0),
          BCrossGradBDotGradAlpha(nAlpha,nZeta,0.0),BCrossGradBDotGradPsi(nAlpha,nZeta,0.0),
          BCrossGradPsiDotGradAlpha(nAlpha,nZeta,0.0);

  
    auto x2Center = 0.0, x3Center = 0.0;
    // Get the equilibrium surface quantities
    auto iota = equilibrium->iota(surfIdx);
    auto dIotads = -0.5*iota/surf*equilibrium->shat(surfIdx);
    // Set the surface dependent values of the equilibrium alpha and zeta
    for (auto alphaIdx = 0; alphaIdx < nAlpha; ++alphaIdx) 
      equilibrium->x2(surfIdx,alphaIdx,std::fmod(x2Center + alphaIdx*2*pi/nAlpha,2*pi));
    // Each field line should be equidistant around x3Center 
    for (auto zetaIdx = 0; zetaIdx < nZeta; ++zetaIdx)
      equilibrium->x3(surfIdx,zetaIdx,x3Center + (zetaIdx-nZeta/2)*2*pi/(nfp*nZeta)/(equilibrium->x3coord() == "theta" ? iota : 1));

    // Determine the theta_vmec value corresponding to each combination of (alpha,zeta)
    matReal thetaVMEC(nAlpha,nZeta);
    for (auto alphaIdx = 0; alphaIdx < nAlpha; ++alphaIdx) {
      auto alpha = equilibrium->x2(surfIdx,alphaIdx);
      for (auto zetaIdx = 0; zetaIdx < nZeta; ++zetaIdx) {
        auto zeta = equilibrium->x3(surfIdx,zetaIdx);
        auto thetaTarget = alpha + iotaSpline(surf) * zeta;
        auto interval = 0.5; 
        thetaVMEC(alphaIdx,zetaIdx) = findVMECTheta(vmec,surf,thetaTarget,interval,zeta);
      }
    }

    // Loop over the Nyquist modes to compute the modified inverse Fourier transform to populate the geometry
    // arrays as a function of (theta_vmec,zeta).  These quantities are then used to compute the PEST transformation.
    // Some quantities are defined using the mnnyq modes and some use only the mn modes, which is contained
    // within the mnnyq modes
    bool nonNyquist = false;
    for (auto mnNyqIdx = 0; mnNyqIdx < vmec->mnmax_nyq(); ++mnNyqIdx) { // Begin loop over poloidal and toroidal mode numbers
      auto mPol = vmec->xmnyq(mnNyqIdx);
      auto nTor = vmec->xnnyq(mnNyqIdx)/nfp;
      int mnIdx = -1;
      if (std::abs(mPol) >= vmec->mPol() || std::abs(nTor) > vmec->nTor()) {
        nonNyquist = false;
      } else {
        nonNyquist = true;
        // Find the non-Nyquist mode number index, mnIdx 
        vecInt xmIdx(vmec->mnmax()), xnIdx(vmec->mnmax());
        int idxCount = -1;
        std::transform(xm.begin(),xm.end(),xmIdx.begin(),[&mPol,idxCount](const int& m)mutable->int { 
            ++idxCount;
            if (m == mPol) {
              return idxCount;
            } else {
              return -1;
            }});
        auto xmEndIter = std::remove(xmIdx.begin(),xmIdx.end(),-1);
        idxCount = -1;
        std::transform(xn.begin(),xn.end(),xnIdx.begin(),[&nTor,&nfp,idxCount](const int& n)mutable->int { 
            ++idxCount;
            if (n == nTor*nfp) {
              return idxCount;
            } else {
              return -1;
            }});
        auto xnEndIter = std::remove(xnIdx.begin(),xnIdx.end(),-1);

        // The non-Nyquist mode is going to be the intersection of the two index sets
        std::vector<int> mnIntersection;
        std::set_intersection(xmIdx.begin(),xmEndIter,xnIdx.begin(),xnEndIter,
            std::back_inserter(mnIntersection));
        if (mnIntersection.size() != 1) {
          throw std::runtime_error("Cannot find the non-Nyquist mode!");
        } else {
          mnIdx = mnIntersection.at(0);
          if (vmec->xm(mnIdx) != mPol || vmec->xn(mnIdx) != nTor*nfp) {
            throw std::runtime_error("xm(mnIdx) != mPol or xn(mnIdx) != nTor!");
          }
        }
      }
      // Set the value of mnIdx to 0 if a Nyquist mode
      // The nonNyquist flag is used to determine if a variable increments or not
      if (!nonNyquist) mnIdx = 0;

      //Compute the radial derivatives
      // Generate references to the columns of the matrices 
      // R and Z are on the full grid with non-Nyquist modes
      auto rmnc_col = column(vmec->rmnc(),mnIdx);
      boost::math::cubic_b_spline rmncSpline(rmnc_col.cbegin(),rmnc_col.cend(),sAxis,surfaceSpacing);
      auto zmns_col = column(vmec->zmns(),mnIdx);
      boost::math::cubic_b_spline zmnsSpline(zmns_col.cbegin(),zmns_col.cend(),sAxis,surfaceSpacing);
      // All the rest of the quantities except B_sub_s are on the half grid
      // Lambda is indexed by non-Nyquist modes
      auto lmns_col = column(vmec->lmns(),mnIdx);
      boost::math::cubic_b_spline lmnsSpline(std::next(lmns_col.cbegin()),lmns_col.cend(),sHalfAxis,surfaceSpacing);
      // All the rest use the Nyquist mode index
      auto bmnc_col = column(vmec->bmnc(),mnNyqIdx);
      boost::math::cubic_b_spline bmncSpline(std::next(bmnc_col.cbegin()),bmnc_col.cend(),sHalfAxis,surfaceSpacing);
      auto gmnc_col = column(vmec->gmnc(),mnNyqIdx);
      boost::math::cubic_b_spline gmncSpline(std::next(gmnc_col.cbegin()),gmnc_col.cend(),sHalfAxis,surfaceSpacing);
      auto bsupumnc_col = column(vmec->bsupumnc(),mnNyqIdx);
      boost::math::cubic_b_spline bsupumncSpline(std::next(bsupumnc_col.cbegin()),bsupumnc_col.cend(),sHalfAxis,surfaceSpacing);
      auto bsupvmnc_col = column(vmec->bsupvmnc(),mnNyqIdx);
      boost::math::cubic_b_spline bsupvmncSpline(std::next(bsupvmnc_col.cbegin()),bsupvmnc_col.cend(),sHalfAxis,surfaceSpacing);
      auto bsubumnc_col = column(vmec->bsubumnc(),mnNyqIdx);
      boost::math::cubic_b_spline bsubumncSpline(std::next(bsubumnc_col.cbegin()),bsubumnc_col.cend(),sHalfAxis,surfaceSpacing);
      auto bsubvmnc_col = column(vmec->bsubvmnc(),mnNyqIdx);
      boost::math::cubic_b_spline bsubvmncSpline(std::next(bsubvmnc_col.cbegin()),bsubvmnc_col.cend(),sHalfAxis,surfaceSpacing);
      auto bsubsmns_col = column(vmec->bsubsmns(),mnNyqIdx);
      boost::math::cubic_b_spline bsubsmnsSpline(bsubsmns_col.cbegin(),bsubsmns_col.cend(),sAxis,surfaceSpacing);

      // With the radial splines defined, compute the value of the geometric quantities at each (alpha,zeta) 
      // Both loops can be perfomed in parallel, implementing with PSTL should lead to performance gains
      for (auto alphaIdx = 0; alphaIdx < nAlpha; ++alphaIdx) {
        for(auto zetaIdx = 0; zetaIdx < nZeta; ++zetaIdx) {
          auto zeta = equilibrium->x3(surfIdx,zetaIdx);
          auto cosAngle = std::cos(mPol * thetaVMEC(alphaIdx,zetaIdx) - nTor * nfp * zeta);
          auto sinAngle = std::sin(mPol * thetaVMEC(alphaIdx,zetaIdx) - nTor * nfp * zeta);

          B(alphaIdx,zetaIdx) += bmncSpline(surf)*cosAngle;
          dBds(alphaIdx,zetaIdx) += bmncSpline.prime(surf)*cosAngle;
          dBdtv(alphaIdx,zetaIdx) -= mPol*bmncSpline(surf)*sinAngle;
          dBdz(alphaIdx,zetaIdx) += nTor*nfp*bmncSpline(surf)*sinAngle;

          jacobian(alphaIdx,zetaIdx) += gmncSpline(surf)*cosAngle;

          Bsuptv(alphaIdx,zetaIdx) += bsupumncSpline(surf)*cosAngle;
          Bsupz(alphaIdx,zetaIdx) += bsupvmncSpline(surf)*cosAngle;
          Bsubtv(alphaIdx,zetaIdx) += bsubumncSpline(surf)*cosAngle;
          Bsubz(alphaIdx,zetaIdx) += bsubvmncSpline(surf)*cosAngle;
          Bsubs(alphaIdx,zetaIdx) += bsubsmnsSpline(surf)*sinAngle;
          
          R(alphaIdx,zetaIdx) += nonNyquist ? rmncSpline(surf)*cosAngle : 0.0;
          dRds(alphaIdx,zetaIdx) += nonNyquist ? rmncSpline.prime(surf)*cosAngle : 0.0;
          dRdtv(alphaIdx,zetaIdx) -= nonNyquist ? mPol*rmncSpline(surf)*sinAngle : 0.0;
          dRdz(alphaIdx,zetaIdx) += nonNyquist ? nTor*nfp*rmncSpline(surf)*sinAngle : 0.0;

          Z(alphaIdx,zetaIdx) += nonNyquist ? zmnsSpline(surf)*sinAngle : 0.0;
          dZds(alphaIdx,zetaIdx) += nonNyquist ? zmnsSpline.prime(surf)*sinAngle : 0.0;
          dZdtv(alphaIdx,zetaIdx) -= nonNyquist ? mPol*zmnsSpline(surf)*cosAngle : 0.0;
          dZdz(alphaIdx,zetaIdx) += nonNyquist ? nTor*nfp*zmnsSpline(surf)*cosAngle : 0.0;

          Lambda(alphaIdx,zetaIdx) += nonNyquist ? lmnsSpline(surf)*sinAngle : 0.0;
          dLds(alphaIdx,zetaIdx) += nonNyquist ? lmnsSpline.prime(surf)*sinAngle : 0.0;
          dLdtv(alphaIdx,zetaIdx) -= nonNyquist ? mPol*lmnsSpline(surf)*cosAngle : 0.0;
          dLdz(alphaIdx,zetaIdx) += nonNyquist ? nTor*nfp*lmnsSpline(surf)*cosAngle : 0.0;
        } // End loop over zeta
      } // End loop over alpha

      // Repeat the above procedure with the asymmetric modes if vmec->lasym() == true
      if (vmec->lasym()) {
        //Compute the radial derivatives
        // Generate references to the columns of the matrices 
        // R and Z are on the full grid with non-Nyquist modes
        auto rmns_col = column(vmec->rmns(),mnIdx);
        boost::math::cubic_b_spline rmnsSpline(rmns_col.cbegin(),rmns_col.cend(),sAxis,surfaceSpacing);
        auto zmnc_col = column(vmec->zmnc(),mnIdx);
        boost::math::cubic_b_spline zmncSpline(zmnc_col.cbegin(),zmnc_col.cend(),sAxis,surfaceSpacing);
        // All the rest of the quantities except B_sub_s are on the half grid
        // Lambda is indexed by non-Nyquist modes
        auto lmnc_col = column(vmec->lmnc(),mnIdx);
        boost::math::cubic_b_spline lmncSpline(std::next(lmnc_col.cbegin()),lmnc_col.cend(),sHalfAxis,surfaceSpacing);
        // All the rest use the Nyquist mode index
        auto bmns_col = column(vmec->bmns(),mnNyqIdx);
        boost::math::cubic_b_spline bmnsSpline(std::next(bmns_col.cbegin()),bmns_col.cend(),sHalfAxis,surfaceSpacing);
        auto gmns_col = column(vmec->gmns(),mnNyqIdx);
        boost::math::cubic_b_spline gmnsSpline(std::next(gmns_col.cbegin()),gmns_col.cend(),sHalfAxis,surfaceSpacing);
        auto bsupumns_col = column(vmec->bsupumns(),mnNyqIdx);
        boost::math::cubic_b_spline bsupumnsSpline(std::next(bsupumns_col.cbegin()),bsupumns_col.cend(),sHalfAxis,surfaceSpacing);
        auto bsupvmns_col = column(vmec->bsupvmns(),mnNyqIdx);
        boost::math::cubic_b_spline bsupvmnsSpline(std::next(bsupvmns_col.cbegin()),bsupvmns_col.cend(),sHalfAxis,surfaceSpacing);
        auto bsubumns_col = column(vmec->bsubumns(),mnNyqIdx);
        boost::math::cubic_b_spline bsubumnsSpline(std::next(bsubumns_col.cbegin()),bsubumns_col.cend(),sHalfAxis,surfaceSpacing);
        auto bsubvmns_col = column(vmec->bsubvmns(),mnNyqIdx);
        boost::math::cubic_b_spline bsubvmnsSpline(std::next(bsubvmns_col.cbegin()),bsubvmns_col.cend(),sHalfAxis,surfaceSpacing);
        auto bsubsmnc_col = column(vmec->bsubsmnc(),mnNyqIdx);
        boost::math::cubic_b_spline bsubsmncSpline(bsubsmnc_col.cbegin(),bsubsmnc_col.cend(),sAxis,surfaceSpacing);

        // Both loops can be performed in parallel, implementing with PSTL should lead to performance gains
        for (auto alphaIdx = 0; alphaIdx < nAlpha; ++alphaIdx) {
          for(auto zetaIdx = 0; zetaIdx < nZeta; ++zetaIdx) {
            auto zeta = equilibrium->x3(surfIdx,zetaIdx);
            auto cosAngle = std::cos(mPol * thetaVMEC(alphaIdx,zetaIdx) - nTor * nfp * zeta);
            auto sinAngle = std::sin(mPol * thetaVMEC(alphaIdx,zetaIdx) - nTor * nfp * zeta);

            B(alphaIdx,zetaIdx) += bmnsSpline(surf)*sinAngle;
            dBds(alphaIdx,zetaIdx) += bmnsSpline.prime(surf)*sinAngle;
            dBdtv(alphaIdx,zetaIdx) -= mPol*bmnsSpline(surf)*cosAngle;
            dBdz(alphaIdx,zetaIdx) += nTor*nfp*bmnsSpline(surf)*cosAngle;

            jacobian(alphaIdx,zetaIdx) += gmnsSpline(surf)*sinAngle;

            Bsuptv(alphaIdx,zetaIdx) += bsupumnsSpline(surf)*sinAngle;
            Bsupz(alphaIdx,zetaIdx) += bsupvmnsSpline(surf)*sinAngle;
            Bsubtv(alphaIdx,zetaIdx) += bsubumnsSpline(surf)*sinAngle;
            Bsubz(alphaIdx,zetaIdx) += bsubvmnsSpline(surf)*sinAngle;
            Bsubs(alphaIdx,zetaIdx) += bsubsmncSpline(surf)*cosAngle;
            
            R(alphaIdx,zetaIdx) += nonNyquist ? rmnsSpline(surf)*sinAngle : 0.0;
            dRds(alphaIdx,zetaIdx) += nonNyquist ? rmnsSpline.prime(surf)*sinAngle : 0.0;
            dRdtv(alphaIdx,zetaIdx) -= nonNyquist ? mPol*rmnsSpline(surf)*cosAngle : 0.0;
            dRdz(alphaIdx,zetaIdx) += nonNyquist ? nTor*nfp*rmnsSpline(surf)*cosAngle : 0.0;

            Z(alphaIdx,zetaIdx) += nonNyquist ? zmncSpline(surf)*cosAngle : 0.0;
            dZds(alphaIdx,zetaIdx) += nonNyquist ? zmncSpline.prime(surf)*cosAngle : 0.0;
            dZdtv(alphaIdx,zetaIdx) -= nonNyquist ? mPol*zmncSpline(surf)*sinAngle : 0.0;
            dZdz(alphaIdx,zetaIdx) += nonNyquist ? nTor*nfp*zmncSpline(surf)*sinAngle : 0.0;

            Lambda(alphaIdx,zetaIdx) += nonNyquist ? lmncSpline(surf)*cosAngle : 0.0;
            dLds(alphaIdx,zetaIdx) += nonNyquist ? lmncSpline.prime(surf)*cosAngle : 0.0;
            dLdtv(alphaIdx,zetaIdx) -= nonNyquist ? mPol*lmncSpline(surf)*sinAngle : 0.0;
            dLdz(alphaIdx,zetaIdx) += nonNyquist ? nTor*nfp*lmncSpline(surf)*sinAngle : 0.0;
          } // End loop over zeta
        } // End loop over alpha

      } // End asymmetric mode evaluation
    } // End loop over poloidal and toroidal mode numbers 

    // At this point, only the arrays like B, dBds, dBdtv,... should still be defined
    // In particular, we now have the cylindrical R(theta_vmec,zeta) and Z(theta_vmec,zeta), so we can now
    // compute the Cartesian components of the gradient basis vectors using the dual relations.
    // The cylindrical system is given by (R,Phi,Z) and the Cartesian by (X,Y,Z) = (R*cos(Phi),R*sin(Phi),Z)
    // and the PEST specification that Phi = -zeta;
    // Start the computing the dual relations 
    for (auto alphaIdx = 0; alphaIdx < nAlpha; ++alphaIdx) {
      for (auto zetaIdx = 0; zetaIdx < nZeta; ++zetaIdx) {
        auto zeta = equilibrium->x3(surfIdx,zetaIdx);
        auto cosAngle = std::cos(zeta);
        auto sinAngle = std::sin(zeta);
        auto sqrt_g_inv = 1.0/jacobian(alphaIdx,zetaIdx);

        // X = R * cos(Phi)
        dXds(alphaIdx,zetaIdx) = dRds(alphaIdx,zetaIdx)*cosAngle;
        dXdtv(alphaIdx,zetaIdx) = dRdtv(alphaIdx,zetaIdx)*sinAngle;
        dXdz(alphaIdx,zetaIdx) = dRdz(alphaIdx,zetaIdx)*cosAngle - R(alphaIdx,zetaIdx)*sinAngle;

        // Y = R * sin(Phi)
        dYds(alphaIdx,zetaIdx) = dRds(alphaIdx,zetaIdx)*sinAngle;
        dYdtv(alphaIdx,zetaIdx) = dRdtv(alphaIdx,zetaIdx)*cosAngle;
        dYdz(alphaIdx,zetaIdx) = dRdz(alphaIdx,zetaIdx)*sinAngle + R(alphaIdx,zetaIdx)*cosAngle;

        grad_s_X(alphaIdx,zetaIdx) = sqrt_g_inv * 
          (dYdtv(alphaIdx,zetaIdx) * dZdz(alphaIdx,zetaIdx) - dZdtv(alphaIdx,zetaIdx) * dYdz(alphaIdx,zetaIdx));
        grad_s_Y(alphaIdx,zetaIdx) = sqrt_g_inv * 
          (dZdtv(alphaIdx,zetaIdx) * dZdz(alphaIdx,zetaIdx) - dXdtv(alphaIdx,zetaIdx) * dZdz(alphaIdx,zetaIdx));
        grad_s_Z(alphaIdx,zetaIdx) = sqrt_g_inv * 
          (dXdtv(alphaIdx,zetaIdx) * dYdz(alphaIdx,zetaIdx) - dYdtv(alphaIdx,zetaIdx) * dXdz(alphaIdx,zetaIdx));

        grad_thetaVMEC_X = sqrt_g_inv *
          (dYdz(alphaIdx,zetaIdx) * dZds(alphaIdx,zetaIdx) - dZdz(alphaIdx,zetaIdx) * dYds(alphaIdx,zetaIdx));
        grad_thetaVMEC_Y = sqrt_g_inv *
          (dZdz(alphaIdx,zetaIdx) * dXds(alphaIdx,zetaIdx) - dXdz(alphaIdx,zetaIdx) * dZds(alphaIdx,zetaIdx));
        grad_thetaVMEC_Z = sqrt_g_inv *
          (dXdz(alphaIdx,zetaIdx) * dYds(alphaIdx,zetaIdx) - dYdz(alphaIdx,zetaIdx) * dXds(alphaIdx,zetaIdx));

        grad_zeta_X = sqrt_g_inv *
          (dYds(alphaIdx,zetaIdx) * dZdtv(alphaIdx,zetaIdx) - dZds(alphaIdx,zetaIdx) * dYdtv(alphaIdx,zetaIdx));
        grad_zeta_Y = sqrt_g_inv *
          (dZds(alphaIdx,zetaIdx) * dXdtv(alphaIdx,zetaIdx) - dXds(alphaIdx,zetaIdx) * dZdtv(alphaIdx,zetaIdx));
        grad_zeta_Z = sqrt_g_inv *
          (dXds(alphaIdx,zetaIdx) * dYdtv(alphaIdx,zetaIdx) - dYds(alphaIdx,zetaIdx) * dXdtv(alphaIdx,zetaIdx));

        grad_psi_X(alphaIdx,zetaIdx) = grad_s_X(alphaIdx,zetaIdx) * edgeFlux2Pi;
        grad_psi_Y(alphaIdx,zetaIdx) = grad_s_Y(alphaIdx,zetaIdx) * edgeFlux2Pi;
        grad_psi_Z(alphaIdx,zetaIdx) = grad_s_Z(alphaIdx,zetaIdx) * edgeFlux2Pi;

        auto grad_s_factor = dLds(alphaIdx,zetaIdx) - dIotads * zeta;
        auto grad_tv_factor = 1.0 + dLdtv(alphaIdx,zetaIdx);
        auto grad_z_factor = dLdz(alphaIdx,zetaIdx) - iota;
        grad_alpha_X(alphaIdx,zetaIdx) = grad_s_X(alphaIdx,zetaIdx) * grad_s_factor +
          grad_thetaVMEC_X(alphaIdx,zetaIdx) * grad_tv_factor + grad_zeta_X(alphaIdx,zetaIdx) * grad_z_factor;
        grad_alpha_Y(alphaIdx,zetaIdx) = grad_s_Y(alphaIdx,zetaIdx) * grad_s_factor +
          grad_thetaVMEC_Y(alphaIdx,zetaIdx) * grad_tv_factor + grad_zeta_Y(alphaIdx,zetaIdx) * grad_z_factor;
        grad_alpha_Z(alphaIdx,zetaIdx) = grad_s_Z(alphaIdx,zetaIdx) * grad_s_factor +
          grad_thetaVMEC_Z(alphaIdx,zetaIdx) * grad_tv_factor + grad_zeta_Z(alphaIdx,zetaIdx) * grad_z_factor;

        grad_theta_X(alphaIdx,zetaIdx) = dLds(alphaIdx,zetaIdx) * grad_s_X(alphaIdx,zetaIdx) + 
          grad_thetaVMEC_X(alphaIdx,zetaIdx) * grad_tv_factor + grad_zeta_X(alphaIdx,zetaIdx) * dLdz(alphaIdx,zetaIdx);
        grad_theta_Y(alphaIdx,zetaIdx) = dLds(alphaIdx,zetaIdx) * grad_s_Y(alphaIdx,zetaIdx) + 
          grad_thetaVMEC_Y(alphaIdx,zetaIdx) * grad_tv_factor + grad_zeta_Y(alphaIdx,zetaIdx) * dLdz(alphaIdx,zetaIdx);
        grad_theta_Z(alphaIdx,zetaIdx) = dLds(alphaIdx,zetaIdx) * grad_s_Z(alphaIdx,zetaIdx) + 
          grad_thetaVMEC_Z(alphaIdx,zetaIdx) * grad_tv_factor + grad_zeta_Z(alphaIdx,zetaIdx) * dLdz(alphaIdx,zetaIdx);

        grad_B_X(alphaIdx,zetaIdx) = dBds(alphaIdx,zetaIdx) * grad_s_X(alphaIdx,zetaIdx) + 
          dBdtv(alphaIdx,zetaIdx) * grad_thetaVMEC_X(alphaIdx,zetaIdx) + dBdz(alphaIdx,zetaIdx) * grad_zeta_X(alphaIdx,zetaIdx);
        grad_B_Y(alphaIdx,zetaIdx) = dBds(alphaIdx,zetaIdx) * grad_s_Y(alphaIdx,zetaIdx) + 
          dBdtv(alphaIdx,zetaIdx) * grad_thetaVMEC_Y(alphaIdx,zetaIdx) + dBdz(alphaIdx,zetaIdx) * grad_zeta_Y(alphaIdx,zetaIdx);
        grad_B_Z(alphaIdx,zetaIdx) = dBds(alphaIdx,zetaIdx) * grad_s_Z(alphaIdx,zetaIdx) + 
          dBdtv(alphaIdx,zetaIdx) * grad_thetaVMEC_Z(alphaIdx,zetaIdx) + dBdz(alphaIdx,zetaIdx) * grad_zeta_Z(alphaIdx,zetaIdx);

        B_X(alphaIdx,zetaIdx) = sqrt_g_inv * edgeFlux2Pi * 
          (dXdz(alphaIdx,zetaIdx)*grad_tv_factor + (iota - dLdz(alphaIdx,zetaIdx)) * dXdtv(alphaIdx,zetaIdx));
        B_Y(alphaIdx,zetaIdx) = sqrt_g_inv * edgeFlux2Pi * 
          (dYdz(alphaIdx,zetaIdx)*grad_tv_factor + (iota - dLdz(alphaIdx,zetaIdx)) * dYdtv(alphaIdx,zetaIdx));
        B_Z(alphaIdx,zetaIdx) = sqrt_g_inv * edgeFlux2Pi * 
          (dZdz(alphaIdx,zetaIdx)*grad_tv_factor + (iota - dLdz(alphaIdx,zetaIdx)) * dZdtv(alphaIdx,zetaIdx));
        // End of dual relation calculation

        // Calculate the component of the B x grad(psi) . grad(alpha) 
        BCrossGradPsiDotGradAlpha(alphaIdx,zetaIdx) = 
          B_Y(alphaIdx,zetaIdx) * grad_psi_Z(alphaIdx,zetaIdx) * grad_alpha_X(alphaIdx,zetaIdx) -
          B_Z(alphaIdx,zetaIdx) * grad_psi_Y(alphaIdx,zetaIdx) * grad_alpha_X(alphaIdx,zetaIdx) +
          B_Z(alphaIdx,zetaIdx) * grad_psi_X(alphaIdx,zetaIdx) * grad_alpha_Y(alphaIdx,zetaIdx) -
          B_X(alphaIdx,zetaIdx) * grad_psi_Z(alphaIdx,zetaIdx) * grad_alpha_Y(alphaIdx,zetaIdx) +
          B_X(alphaIdx,zetaIdx) * grad_psi_Y(alphaIdx,zetaIdx) * grad_alpha_Z(alphaIdx,zetaIdx) -
          B_Y(alphaIdx,zetaIdx) * grad_psi_X(alphaIdx,zetaIdx) * grad_alpha_Z(alphaIdx,zetaIdx);
       
        // Calculate the component of the B x grad(B) . grad(psi) 
        BCrossGradBDotGradPsi(alphaIdx,zetaIdx) = 
          B_Y(alphaIdx,zetaIdx) * grad_B_Z(alphaIdx,zetaIdx) * grad_psi_X(alphaIdx,zetaIdx) -
          B_Z(alphaIdx,zetaIdx) * grad_B_Y(alphaIdx,zetaIdx) * grad_psi_X(alphaIdx,zetaIdx) +
          B_Z(alphaIdx,zetaIdx) * grad_B_X(alphaIdx,zetaIdx) * grad_psi_Y(alphaIdx,zetaIdx) -
          B_X(alphaIdx,zetaIdx) * grad_B_Z(alphaIdx,zetaIdx) * grad_psi_Y(alphaIdx,zetaIdx) +
          B_X(alphaIdx,zetaIdx) * grad_B_Y(alphaIdx,zetaIdx) * grad_psi_Z(alphaIdx,zetaIdx) -
          B_Y(alphaIdx,zetaIdx) * grad_B_X(alphaIdx,zetaIdx) * grad_psi_Z(alphaIdx,zetaIdx);

        // Calculate the component of the B x grad(B) . grad(alpha) 
        BCrossGradBDotGradAlpha(alphaIdx,zetaIdx) = 
          B_Y(alphaIdx,zetaIdx) * grad_B_Z(alphaIdx,zetaIdx) * grad_alpha_X(alphaIdx,zetaIdx) -
          B_Z(alphaIdx,zetaIdx) * grad_B_Y(alphaIdx,zetaIdx) * grad_alpha_X(alphaIdx,zetaIdx) +
          B_Z(alphaIdx,zetaIdx) * grad_B_X(alphaIdx,zetaIdx) * grad_alpha_Y(alphaIdx,zetaIdx) -
          B_X(alphaIdx,zetaIdx) * grad_B_Z(alphaIdx,zetaIdx) * grad_alpha_Y(alphaIdx,zetaIdx) +
          B_X(alphaIdx,zetaIdx) * grad_B_Y(alphaIdx,zetaIdx) * grad_alpha_Z(alphaIdx,zetaIdx) -
          B_Y(alphaIdx,zetaIdx) * grad_B_X(alphaIdx,zetaIdx) * grad_alpha_Z(alphaIdx,zetaIdx);


        // Populate the EqObject data structures
        // First populate the quantities that are agnostic of the field-line following coordinate  
        equilibrium->Bmag(surfIdx,alphaIdx,zetaIdx,B(alphaIdx,zetaIdx));
        equilibrium->g11(surfIdx,alphaIdx,zetaIdx,4.0/(std::pow(Bref,2)*std::pow(Lref,4))*
            (std::pow(grad_psi_X(alphaIdx,zetaIdx),2) + std::pow(grad_psi_Y(alphaIdx,zetaIdx),2) + std::pow(grad_psi_Z(alphaIdx,zetaIdx),2)));
        equilibrium->g12(surfIdx,alphaIdx,zetaIdx,2.0*fluxSign/(Bref*std::pow(Lref,2)) * 
            (grad_psi_X(alphaIdx,zetaIdx) * grad_psi_X(alphaIdx,zetaIdx) + grad_psi_Y(alphaIdx,zetaIdx) * grad_alpha_Y(alphaIdx,zetaIdx) + grad_psi_Z(alphaIdx,zetaIdx) * grad_alpha_Z(alphaIdx,zetaIdx)));
        equilibrium->g22(surfIdx,alphaIdx,zetaIdx,std::pow(grad_psi_X(alphaIdx,zetaIdx),2) + std::pow(grad_alpha_Y(alphaIdx,zetaIdx),2) + std::pow(grad_alpha_Z(alphaIdx,zetaIdx),2));
        equilibrium->dBdx1(surfIdx,alphaIdx,zetaIdx,fluxSign*BCrossGradBDotGradAlpha(alphaIdx,zetaIdx)/B(alphaIdx,zetaIdx) +
            2.0*mu0/(Bref*std::pow(Lref,2)) * pressureSpline.prime(surf) * 
            BCrossGradPsiDotGradAlpha(alphaIdx,zetaIdx)/std::pow(B(alphaIdx,zetaIdx),2));
        equilibrium->dBdx2(surfIdx,alphaIdx,zetaIdx,2.0*fluxSign/(Bref*std::pow(Lref,2)) *
            BCrossGradBDotGradPsi(alphaIdx,zetaIdx) / B(alphaIdx,zetaIdx));
        
        // The choice of x3coord determines the value of all of the metric elements involving the third coordinate
        // as well as the parallel derivative of the magnitude of B
        if (equilibrium->x3coord() == "zeta") {
          equilibrium->jac(surfIdx,alphaIdx,zetaIdx,std::abs(jacobian(alphaIdx,zetaIdx)));
          equilibrium->g13(surfIdx,alphaIdx,zetaIdx,2.0*fluxSign/(Bref*std::pow(Lref,2)) *
               (grad_psi_X(alphaIdx,zetaIdx) * grad_zeta_X(alphaIdx,zetaIdx) + grad_psi_X(alphaIdx,zetaIdx) * grad_zeta_Y(alphaIdx,zetaIdx) + grad_psi_Z(alphaIdx,zetaIdx) * grad_zeta_Z(alphaIdx,zetaIdx)));
          equilibrium->g23(surfIdx,alphaIdx,zetaIdx,grad_alpha_X(alphaIdx,zetaIdx) * grad_zeta_X(alphaIdx,zetaIdx) + grad_alpha_X(alphaIdx,zetaIdx) * grad_zeta_Y(alphaIdx,zetaIdx) + grad_alpha_Z(alphaIdx,zetaIdx) * grad_zeta_Z(alphaIdx,zetaIdx));
          equilibrium->g23(surfIdx,alphaIdx,zetaIdx,std::pow(grad_zeta_X(alphaIdx,zetaIdx),2) + std::pow(grad_zeta_X(alphaIdx,zetaIdx),2) + std::pow(grad_zeta_Z(alphaIdx,zetaIdx),2));
          equilibrium->dBdx3(surfIdx,alphaIdx,zetaIdx,dBdz(alphaIdx,zetaIdx));
        }
        if (equilibrium->x3coord() == "theta") {
          equilibrium->jac(surfIdx,alphaIdx,zetaIdx,std::abs(jacobian(alphaIdx,zetaIdx)/(1.0+dLdtv(alphaIdx,zetaIdx))));
          equilibrium->g13(surfIdx,alphaIdx,zetaIdx,2.0*fluxSign/(Bref*std::pow(Lref,2)) *
               (grad_psi_X(alphaIdx,zetaIdx) * grad_theta_X(alphaIdx,zetaIdx) + grad_psi_X(alphaIdx,zetaIdx) * grad_theta_Y(alphaIdx,zetaIdx) + grad_psi_Z(alphaIdx,zetaIdx) * grad_theta_Z(alphaIdx,zetaIdx)));
          equilibrium->g23(surfIdx,alphaIdx,zetaIdx,grad_alpha_X(alphaIdx,zetaIdx) * grad_theta_X(alphaIdx,zetaIdx) + grad_alpha_X(alphaIdx,zetaIdx) * grad_theta_Y(alphaIdx,zetaIdx) + grad_alpha_Z(alphaIdx,zetaIdx) * grad_theta_Z(alphaIdx,zetaIdx));
          equilibrium->g23(surfIdx,alphaIdx,zetaIdx,std::pow(grad_theta_X(alphaIdx,zetaIdx),2) + std::pow(grad_theta_X(alphaIdx,zetaIdx),2) + std::pow(grad_theta_Z(alphaIdx,zetaIdx),2));
          equilibrium->dBdx3(surfIdx,alphaIdx,zetaIdx,dBdtv(alphaIdx,zetaIdx)/grad_tv_factor+dBdz(alphaIdx,zetaIdx)/iota);
        }

        equilibrium->cylR(surfIdx,alphaIdx,zetaIdx,R(alphaIdx,zetaIdx));
        equilibrium->cylZ(surfIdx,alphaIdx,zetaIdx,Z(alphaIdx,zetaIdx));
        equilibrium->cylTheta(surfIdx,alphaIdx,zetaIdx,-zeta);

        equilibrium->X(surfIdx,alphaIdx,zetaIdx,R(alphaIdx,zetaIdx)*std::cos(-zeta));
        equilibrium->Y(surfIdx,alphaIdx,zetaIdx,R(alphaIdx,zetaIdx)*std::sin(-zeta));
        equilibrium->Z(surfIdx,alphaIdx,zetaIdx,Z(alphaIdx,zetaIdx));
      }
    }
  } // End global surface loop
}

real Transforms::PEST::findVMECTheta(const std::shared_ptr<VMEC>& vmec, const real& surf, const real& thetaTarget, const real& interval, const real& zeta) {
  // The PEST theta value is given by theta_pest = alpha + iota*zeta, alpha is given by x2 in the EqObject
  // Want to find the zero of theta_vmec + Lambda(theta_vmec,zeta) = theta_pest = alpha + iota*zeta
  // Lambda is defined on the half-mesh, need to compute the radial weights associated with the adjacent surfaces 
  // Currently this performed by a linear interpolation
  std::pair<int,real> lowerSurf = std::make_pair((int)std::floor(surf*(vmec->nSurf()-1)-0.5),surf*(vmec->nSurf()-1) - 0.5 - std::floor(surf*(vmec->nSurf()-1)-0.5));
  std::pair<int,real> upperSurf = std::make_pair(lowerSurf.first+1,1.0-lowerSurf.second);
  vecInt mnIndices(vmec->mnmax());
  std::iota(mnIndices.begin(),mnIndices.end(),0);

  // Define the residual function as a C++ lambda capturing everything in the scope 
  std::function<real(real)> thetaVMECResidual = [&](const real& x) {
    return std::transform_reduce(mnIndices.cbegin(),mnIndices.cend(),x - thetaTarget,std::plus<>(),[&](const int& mnIdx) {
      if(!vmec->lasym()) {
        return std::sin(vmec->xm(mnIdx)*x - vmec->xn(mnIdx)*zeta)*(vmec->lmns(lowerSurf.first,mnIdx)*lowerSurf.second +
          vmec->lmns(upperSurf.first,mnIdx)*upperSurf.second);
      } else {
        return std::sin(vmec->xm(mnIdx)*x - vmec->xn(mnIdx)*zeta)*(vmec->lmns(lowerSurf.first,mnIdx)*lowerSurf.second +
          vmec->lmns(upperSurf.first,mnIdx)*upperSurf.second) + std::cos(vmec->xm(mnIdx)*x - vmec->xn(mnIdx)*zeta)*
          (vmec->lmnc(lowerSurf.first,mnIdx)*lowerSurf.second + vmec->lmnc(upperSurf.first,mnIdx)*upperSurf.second);
      }
    });
  };
  // Compute the residual value at the upper and lower boundaries
  auto boundLower = thetaVMECResidual(thetaTarget-0.5*interval);
  auto boundUpper = thetaVMECResidual(thetaTarget+0.5*interval);
  
  
  // The root finding method requires boundLower*boundUpper < 0
  if (boundLower*boundUpper >= 0) {
    return findVMECTheta(vmec,surf,thetaTarget,2*interval,zeta);
  } else {
    // Attempt to solve for the residual = zero on the domain with the the Boost TOMS 748 algorithm
    try {
      const int digits = std::numeric_limits<real>::digits;
      const int get_digits = digits - 3;
      boost::math::tools::eps_tolerance<real> tol(get_digits);
      boost::uintmax_t it = 500;
      std::pair<real,real> root = boost::math::tools::toms748_solve(thetaVMECResidual, thetaTarget-0.5*interval, thetaTarget+0.5*interval, tol, it);
      // TODO: This algorithm will not work if the number iterations isn't large enough, need to fix
      return root.first + 0.5*(root.first + root.second);
    } catch (boost::wrapexcept<std::domain_error>&) {
      // Broaden the interval and try again 
      return findVMECTheta(vmec,surf,thetaTarget,2*interval,zeta);
    }
  }
}

EqObject Transforms::PEST::geneNormalizations(const std::shared_ptr<EqObject>& eqOld) {
  EqObject equilibrium = *eqOld;
  const real Lref = equilibrium.Lref();
  const real Bref = equilibrium.Bref();
  for (auto surfIdx = 0; surfIdx < equilibrium.nx1(); ++surfIdx) {
    const real s0 = equilibrium.x1(surfIdx);
    auto pestDims = equilibrium.nx(surfIdx);
    for (auto alphaIdx = 0; alphaIdx < std::get<0>(pestDims); ++alphaIdx) {
      for (auto zetaIdx = 0; zetaIdx < std::get<1>(pestDims); ++zetaIdx) {
        equilibrium.g11(surfIdx,alphaIdx,zetaIdx,Lref*Lref/(4.0*s0)*eqOld->g11(surfIdx,alphaIdx,zetaIdx));
        equilibrium.g12(surfIdx,alphaIdx,zetaIdx,0.5*Lref*Lref*eqOld->g12(surfIdx,alphaIdx,zetaIdx));
        equilibrium.g22(surfIdx,alphaIdx,zetaIdx,Lref*Lref*s0*eqOld->g22(surfIdx,alphaIdx,zetaIdx));
        equilibrium.g13(surfIdx,alphaIdx,zetaIdx,0.5*Lref*Lref/std::sqrt(s0)*eqOld->g13(surfIdx,alphaIdx,zetaIdx));
        equilibrium.g23(surfIdx,alphaIdx,zetaIdx,Lref*Lref*sqrt(s0)*eqOld->g23(surfIdx,alphaIdx,zetaIdx));
        equilibrium.g33(surfIdx,alphaIdx,zetaIdx,Lref*Lref*eqOld->g33(surfIdx,alphaIdx,zetaIdx));
        equilibrium.dBdx1(surfIdx,alphaIdx,zetaIdx,Lref*Lref*std::sqrt(s0)*eqOld->dBdx1(surfIdx,alphaIdx,zetaIdx)/eqOld->Bmag(surfIdx,alphaIdx,zetaIdx));
        equilibrium.dBdx2(surfIdx,alphaIdx,zetaIdx,0.5*Lref*Lref/std::sqrt(s0)*eqOld->dBdx2(surfIdx,alphaIdx,zetaIdx)/eqOld->Bmag(surfIdx,alphaIdx,zetaIdx));
        equilibrium.dBdx3(surfIdx,alphaIdx,zetaIdx,Lref*eqOld->dBdx3(surfIdx,alphaIdx,zetaIdx)/(eqOld->jac(surfIdx,alphaIdx,zetaIdx)*eqOld->Bmag(surfIdx,alphaIdx,zetaIdx)));
        equilibrium.Bmag(surfIdx,alphaIdx,zetaIdx,eqOld->Bmag(surfIdx,alphaIdx,zetaIdx)/Bref);
        equilibrium.jac(surfIdx,alphaIdx,zetaIdx,2.0/eqOld->iota(surfIdx)/std::pow(Lref,2)*eqOld->jac(surfIdx,alphaIdx,zetaIdx));
      }
    }
  }
  return equilibrium;
}

#include <iostream>

#include "blaze/math/Row.h"

#include "Utils.hpp"
#include "Equilibrium.hpp"

// Define the commonly used constructor for the flux surface
EqObject::FluxSurface::FluxSurface(const int& nx2, const int& nx3) {
  _nx = std::make_tuple(nx2,nx3);
  _x2.resize(nx2);
  _x3.resize(nx3);

  _g11.resize(nx2,nx3);
  _g12.resize(nx2,nx3);
  _g22.resize(nx2,nx3);
  _g13.resize(nx2,nx3);
  _g23.resize(nx2,nx3);
  _g33.resize(nx2,nx3);
  _dBdx1.resize(nx2,nx3);
  _dBdx2.resize(nx2,nx3);
  _dBdx3.resize(nx2,nx3);
  _jac.resize(nx2,nx3);
  _Bmag.resize(nx2,nx3);

  _cylR.resize(nx2,nx3);
  _cylZ.resize(nx2,nx3);
  _cylTheta.resize(nx2,nx3);

  _X.resize(nx2,nx3);
  _Y.resize(nx2,nx3);
  _Z.resize(nx2,nx3);
}

EqObject::FluxSurface::FluxSurface(FluxSurface&& refSurf) : 
  _nx(std::move(refSurf._nx)),
  _x1(std::move(refSurf._x1)),
  _iota(std::move(refSurf._iota)),
  _shat(std::move(refSurf._shat)),
  _nfp(std::move(refSurf._nfp)),
  _x2(std::move(refSurf._x2)),
  _x3(std::move(refSurf._x3)),
  _g11(std::move(refSurf._g11)),
  _g12(std::move(refSurf._g12)),
  _g22(std::move(refSurf._g22)),
  _g13(std::move(refSurf._g13)),
  _g23(std::move(refSurf._g23)),
  _g33(std::move(refSurf._g33)),
  _Bmag(std::move(refSurf._Bmag)),
  _jac(std::move(refSurf._jac)),
  _dBdx1(std::move(refSurf._dBdx1)),
  _dBdx2(std::move(refSurf._dBdx2)),
  _dBdx3(std::move(refSurf._dBdx3)),
  _cylR(std::move(refSurf._cylR)),
  _cylZ(std::move(refSurf._cylZ)),
  _cylTheta(std::move(refSurf._cylTheta)),
  _X(std::move(refSurf._X)),
  _Y(std::move(refSurf._Y)),
  _Z(std::move(refSurf._Z)) {
}

EqObject::FluxSurface::FluxSurface(const FluxSurface&& refSurf) : 
  _nx(std::move(refSurf._nx)),
  _x1(std::move(refSurf._x1)),
  _iota(std::move(refSurf._iota)),
  _shat(std::move(refSurf._shat)),
  _nfp(std::move(refSurf._nfp)),
  _x2(std::move(refSurf._x2)),
  _x3(std::move(refSurf._x3)),
  _g11(std::move(refSurf._g11)),
  _g12(std::move(refSurf._g12)),
  _g22(std::move(refSurf._g22)),
  _g13(std::move(refSurf._g13)),
  _g23(std::move(refSurf._g23)),
  _g33(std::move(refSurf._g33)),
  _Bmag(std::move(refSurf._Bmag)),
  _jac(std::move(refSurf._jac)),
  _dBdx1(std::move(refSurf._dBdx1)),
  _dBdx2(std::move(refSurf._dBdx2)),
  _dBdx3(std::move(refSurf._dBdx3)),
  _cylR(std::move(refSurf._cylR)),
  _cylZ(std::move(refSurf._cylZ)),
  _cylTheta(std::move(refSurf._cylTheta)),
  _X(std::move(refSurf._X)),
  _Y(std::move(refSurf._Y)),
  _Z(std::move(refSurf._Z)) {
}

EqObject::FluxSurface::FluxSurface(const FluxSurface& surfOld) {
  _nx = surfOld.nx();
  _x1 = surfOld.x1();
  _iota = surfOld.iota();
  _shat = surfOld.shat();
  _nfp = surfOld.nfp();

  _x2.resize(std::get<0>(_nx));
  _x3.resize(std::get<1>(_nx));

  _g11.resize(std::get<0>(_nx),std::get<1>(_nx));
  _g12.resize(std::get<0>(_nx),std::get<1>(_nx));
  _g22.resize(std::get<0>(_nx),std::get<1>(_nx));
  _g13.resize(std::get<0>(_nx),std::get<1>(_nx));
  _g23.resize(std::get<0>(_nx),std::get<1>(_nx));
  _g33.resize(std::get<0>(_nx),std::get<1>(_nx));
  _Bmag.resize(std::get<0>(_nx),std::get<1>(_nx));
  _jac.resize(std::get<0>(_nx),std::get<1>(_nx));
  _dBdx1.resize(std::get<0>(_nx),std::get<1>(_nx));
  _dBdx2.resize(std::get<0>(_nx),std::get<1>(_nx));
  _dBdx3.resize(std::get<0>(_nx),std::get<1>(_nx));

  _cylR.resize(std::get<0>(_nx),std::get<1>(_nx));
  _cylZ.resize(std::get<0>(_nx),std::get<1>(_nx));
  _cylTheta.resize(std::get<0>(_nx),std::get<1>(_nx));

  _X.resize(std::get<0>(_nx),std::get<1>(_nx));
  _Y.resize(std::get<0>(_nx),std::get<1>(_nx));
  _Z.resize(std::get<0>(_nx),std::get<1>(_nx));

  for (auto idx2 = 0; idx2 < std::get<0>(_nx); ++idx2) {
    _x2[idx2] = surfOld.x2(idx2);
    for (auto idx3 = 0; idx3 < std::get<1>(_nx); ++idx3) {
      _g11(idx2,idx3) = surfOld.g11(idx2,idx3);
      _g12(idx2,idx3) = surfOld.g12(idx2,idx3);
      _g22(idx2,idx3) = surfOld.g22(idx2,idx3);
      _g13(idx2,idx3) = surfOld.g13(idx2,idx3);
      _g23(idx2,idx3) = surfOld.g23(idx2,idx3);
      _g33(idx2,idx3) = surfOld.g33(idx2,idx3);
      _Bmag(idx2,idx3) = surfOld.Bmag(idx2,idx3);
      _jac(idx2,idx3) = surfOld.jac(idx2,idx3);
      _dBdx1(idx2,idx3) = surfOld.dBdx1(idx2,idx3);
      _dBdx2(idx2,idx3) = surfOld.dBdx2(idx2,idx3);
      _dBdx3(idx2,idx3) = surfOld.dBdx3(idx2,idx3);

      _cylR(idx2,idx3) = surfOld.cylR(idx2,idx3);
      _cylZ(idx2,idx3) = surfOld.cylZ(idx2,idx3);
      _cylTheta(idx2,idx3) = surfOld.cylTheta(idx2,idx3);

      _X(idx2,idx3) = surfOld.X(idx2,idx3);
      _Y(idx2,idx3) = surfOld.Y(idx2,idx3);
      _Z(idx2,idx3) = surfOld.Z(idx2,idx3);
    }
  }
  for (auto idx3 = 0; idx3 < std::get<1>(_nx); ++idx3) _x3[idx3] = surfOld.x3(idx3);

}

// Define the Interpolator struct constructor
EqObject::FluxSurface::Interpolator::Interpolator(FluxSurface* fs) {
  _surface = fs;
  _nRows = std::get<0>(fs->nx());
  _nCols = std::get<1>(fs->nx());
  _rowVals.resize(_nRows);
}

real EqObject::FluxSurface::Interpolator::operator()(dataFuncPtr dataPtr, const real& x2, const real& x3) {
  // Get the value of the x4 within the flux surface field period (EqObject::_nfp)
  auto x3_fp = std::fmod(x3,2*pi/_surface->nfp()-2*pi/(_surface->nfp()*_nCols));
  auto x2_fp = std::fmod(x2,2*pi-2*pi/_nRows);
  // Call the bound operator to get a reference to the data on the flux surface
  const matReal& data = (_surface->*dataPtr)();
  for (auto rowIdx = 0; rowIdx < _nRows; ++rowIdx) {
    auto dataRow = row(data,rowIdx);
    boost::math::cubic_b_spline rowSpline(dataRow.cbegin(),dataRow.cend(),0.0,2*pi/(_nCols*_surface->nfp()));
    _rowVals[rowIdx] = rowSpline(x3_fp);
  }
  boost::math::cubic_b_spline aggregatedSpline(_rowVals.cbegin(),_rowVals.cend(),0.0,2*pi/_nRows);
  return aggregatedSpline(x2_fp);
}

/*
void EqObject::FluxSurface::interpolateFieldline(const int& flIdx) {
  _flines.emplace_back(Fieldline(48));
  Interpolator interpolator(this);
  vecReal zeta(48);
  for (auto i = 0; i<48; ++i) {
    zeta[i] = i*2*pi/(4*48);
    DebugMsg(std::cout << zeta[i] << '\n';);
  }
  const real alpha0 = 0.1;
  dataFuncPtr ptr = &FluxSurface::Bmag;
  for (auto i = 0; i<48; ++i) {
    _flines.at(0).Bmag(i,interpolator(ptr,alpha0,zeta[i]));
    std::cout << _flines.at(0).Bmag(i) << '\n';
    std::cout << "====" << '\n';
  }
}
*/

#include <string>
#include <algorithm>
#include <exception>

#include "netcdf"
#include "blaze/math/Row.h"
#include "boost/math/interpolators/cubic_b_spline.hpp"

#include "VMEC.hpp"
#include "Utils.hpp"

void VMEC::readNetCDF4(const std::string& filename) {
  _filename  = filename;
  // Open the NetCDF VMEC file in read-only mode 
  netCDF::NcFile woutFile(filename,netCDF::NcFile::read);
  
  // Populate the map of wout variables
  auto varMap = woutFile.getVars();
  //std::for_each(std::execution::par, varMap.begin(), varMap.end(),
  std::for_each(varMap.begin(), varMap.end(),
      [this](const std::pair<std::string,netCDF::NcVar>& varIter) {this->populateVar(varIter);});

  // netCDF::NcFile destructor (file close) called upon exit, no need to explicity call ~netCDF::NcFile
}

void VMEC::populateVar(const std::pair<std::string,netCDF::NcVar>& varIter) {
  auto varName = std::get<0>(varIter); 
  auto ncVar = std::get<1>(varIter);
  auto varDims = ncVar.getDims();
  std::cout << varName << '\t';
  std::for_each(varDims.begin(),varDims.end(),[](const netCDF::NcDim& dim){ std::cout << dim.getSize() << '\t';});
  std::cout << '\n';
  
  
  if (varName == "rmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _rmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _rmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "rmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _rmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _rmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "zmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _zmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _zmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "zmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _zmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _zmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }
  if (varName == "bmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "gmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _gmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _gmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "gmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _gmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _gmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "lmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _lmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _lmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "lmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _lmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _lmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsubsmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsubsmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsubsmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsubsmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsubsmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsubsmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }


  if (varName == "bsubumnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsubumnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsubumnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsubumns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsubumns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsubumns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsubvmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsubvmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsubvmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsubvmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsubvmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsubvmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }
  
  if (varName == "bsupumnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsupumnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsupumnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsupumns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsupumns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsupumns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsupvmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsupvmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsupvmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "bsupvmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _bsupvmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _bsupvmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "currumnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _currumnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _currumnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "currumns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _currumns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _currumns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "currvmnc") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _currvmnc.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _currvmnc(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "currvmns") {
    auto dim1 = ncVar.getDim(0).getSize();
    auto dim2 = ncVar.getDim(1).getSize();
    std::vector<real> data(dim1*dim2);
    ncVar.getVar(data.data());
    _currvmns.resize(dim1,dim2);
    for (std::size_t idx=0; idx < dim1*dim2; ++idx) {
      int j = idx/dim2;
      int k = idx % dim2;
      _currvmns(j,k) = data.at(idx);
    }
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "xm") {
    auto dim1 = ncVar.getDim(0).getSize();
    _xm.resize(dim1);
    ncVar.getVar(_xm.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "xn") {
    auto dim1 = ncVar.getDim(0).getSize();
    _xn.resize(dim1);
    ncVar.getVar(_xn.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "xm_nyq") {
    auto dim1 = ncVar.getDim(0).getSize();
    _xmnyq.resize(dim1);
    ncVar.getVar(_xmnyq.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "xn_nyq") {
    auto dim1 = ncVar.getDim(0).getSize();
    _xnnyq.resize(dim1);
    ncVar.getVar(_xnnyq.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "phi") {
    auto dim1 = ncVar.getDim(0).getSize();
    _phi.resize(dim1);
    ncVar.getVar(_phi.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "phips") {
    auto dim1 = ncVar.getDim(0).getSize();
    _phipFull.resize(dim1);
    ncVar.getVar(_phipFull.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "phipf") {
    auto dim1 = ncVar.getDim(0).getSize();
    _phipHalf.resize(dim1);
    ncVar.getVar(_phipHalf.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "iotas") {
    auto dim1 = ncVar.getDim(0).getSize();
    _iotaFull.resize(dim1);
    ncVar.getVar(_iotaFull.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "iotaf") {
    auto dim1 = ncVar.getDim(0).getSize();
    _iotaHalf.resize(dim1);
    ncVar.getVar(_iotaHalf.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "pres") {
    auto dim1 = ncVar.getDim(0).getSize();
    _presFull.resize(dim1);
    ncVar.getVar(_presFull.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "presf") {
    auto dim1 = ncVar.getDim(0).getSize();
    _presHalf.resize(dim1);
    ncVar.getVar(_presHalf.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "betaVol") {
    auto dim1 = ncVar.getDim(0).getSize();
    _betaVol.resize(dim1);
    ncVar.getVar(_betaVol.data());
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "ns") {
    ncVar.getVar(&_nSurf);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "mpol") {
    ncVar.getVar(&_mPol);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "ntor") {
    ncVar.getVar(&_nTor);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "lasym") {
    ncVar.getVar(&_lasym);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "signgs") {
    ncVar.getVar(&_signgs);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "Aminor_p") {
    ncVar.getVar(&_AMinor);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "RMajor_p") {
    ncVar.getVar(&_RMajor);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "betaxis") {
    ncVar.getVar(&_betaAxis);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "betapol") {
    ncVar.getVar(&_betaPol);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "betator") {
    ncVar.getVar(&_betaTor);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "betatotal") {
    ncVar.getVar(&_betaTotal);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "mnmax") {
    ncVar.getVar(&_mnmax);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "mnmax_nyq") {
    ncVar.getVar(&_mnmax_nyq);
    // Exit routine rather than checking other cases
    return;
  }

  if (varName == "nfp") {
    ncVar.getVar(&_nfp);
    // Exit routine rather than checking other cases
    return;
  }

}

void VMEC::checkVMEC() {

  // There is a bug in libstell read_wout_file for ASCII-format wout files, in which the xm_nyq and xn_nyq arrays are sometimes
  // not populated. The next few lines here provide a workaround:
  if (max(_xmnyq) < 1.0 && max(_xnnyq) < 1.0) {
    if (_mnmax_nyq == _mnmax) {
      Utils::warningMsg("xm_nyq and xn_nyq arrays are not populated in the wout file. Using xm and xn instead.\n");
      _xmnyq = _xm;
      _xnnyq = _xn;
    } else {
      throw std::runtime_error("Error! xm_nyq and xn_nyq arrays are not populated in the wout file, and mnmax_nyq != mnmax!");
    }
  }
  // --------------------------------------------------------------------------------
  // Do some sanity checking to ensure the VMEC arrays have some expected properties.
  // --------------------------------------------------------------------------------

  // 'phi' is vmec's array of the toroidal flux (not divided by 2pi!) on vmec's radial grid.
  if (std::abs(_phi[0]) > 1e-14) {
      throw std::runtime_error("Error! VMEC phi array does not begin with 0!");
  }

  auto deltaPhi = _phi[1] - _phi[0];

  for (auto surfIdx = 2; surfIdx < _nSurf; ++surfIdx) {
    if (std::abs(_phi[surfIdx] - _phi[surfIdx-1] - deltaPhi) > 1e-11) {
      throw std::runtime_error("Error! VMEC phi array is not uniformly spaced!");
    }
  }

  for (auto surfIdx = 1; surfIdx < _nSurf; ++surfIdx) {
    if (std::abs(_phipFull[surfIdx] + _phi[_nSurf-1] /(2*pi)) > 1e-11) {
      throw std::runtime_error("Error! VMEC phips array is not constant and equal to -phi(nSurf)/(2*pi)!");
    }
  }

  // The first mode in the m and n arrays should be m=n=0:
  if (_xm[0] != 0) throw std::runtime_error("First element of xm in the wout file should be 0!");
  if (_xn[0] != 0) throw std::runtime_error("First element of xn in the wout file should be 0!");
  if (_xmnyq[0] != 0) throw std::runtime_error("First element of xmnyq in the wout file should be 0!");
  if (_xnnyq[0] != 0) throw std::runtime_error("First element of xnnyq in the wout file should be 0!");
  
  if (_lasym) {
    auto lambdaZero = row(_lmnc,0);
    for (auto iter=lambdaZero.cbegin(); iter != lambdaZero.cend(); ++iter) {
      if (std::abs(*iter) > 0.0) {
        throw std::runtime_error("Error! Expected lmnc to be on the half mesh, but the value at radial index 0 is non-zero!");
      }
    }
  }

  // Lambda should be on the half mesh, so its value at radial index 0 should be 0 for all (m,n)
  auto lambdaZero = row(_lmns,0);
  for (auto iter=lambdaZero.cbegin(); iter != lambdaZero.cend(); ++iter) {
    if (std::abs(*iter) > 0) {
      throw std::runtime_error("Error! Expected lmns to be on the half mesh, but the value at radial index 0 is non-zero!");
    }
  }

  // End consistency checks
}

std::pair<real,real> VMEC::computeIotaShatPair(const real& s) {
  const real ds = 1.0/(_nSurf-1);
  boost::math::cubic_b_spline iotaSpline(_iotaHalf.cbegin(),_iotaHalf.cend(),0.0,ds);
  return std::make_pair(iotaSpline(s),-2.0*s/iotaSpline(s)*iotaSpline.prime(s));
}

#include <iostream>
#include <cstdlib>

#include "netcdf.h"

#include "Utils.hpp"

void Utils::errorHandlerNC(const int status) {
  if (status != NC_NOERR) {
    std::string msg(nc_strerror(status));
    std::cerr << msg << '\n';
    std::exit(EXIT_FAILURE);
  }
}

void Utils::warningMsg(const std::string& warning) {
  std::cout << "!!!!! WARNING !!!!!\n";
  std::cout << warning << '\n';
  std::cout << "!!!!!!!!!!!!!!!!!!!\n";
}

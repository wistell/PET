BASE_DIR:=toolkit
SRC_DIR:=$(addprefix $(BASE_DIR)/,src)
INC_DIR:=$(addprefix $(BASE_DIR)/,include)
BUILD_DIR:=$(addprefix $(BASE_DIR)/,build)
$(shell mkdir -p $(BUILD_DIR))
TEST_DIR:=$(addprefix $(BASE_DIR)/,test)

LIB_DIR:=lib
$(shell mkdir -p $(LIB_DIR))
BIN_DIR:=bin
$(shell mkdir -p $(BIN_DIR))

CXX_SRC := $(shell find $(SRC_DIR) -name '*.cpp' | sed "s|^\./||")
CXX_OBJ := $(addprefix $(BUILD_DIR)/,$(subst .cpp,.o,$(notdir $(CXX_SRC))))
CXX_LIB := $(addprefix $(BUILD_DIR)/,libpet.a)
TEST_CXX := $(shell find $(TEST_DIR) -name '*.cpp' | sed "s|&\./||")
TEST_EXE := $(addprefix $(BIN_DIR)/,$(subst .cpp,,$(notdir $(TEST_CXX))))
LINK_LIB := $(addprefix -l,pet)

PRECOMP :=
CXX_COMPILE_FLAGS:= -m64
DEBUG = 0
ifeq ($(DEBUG),1)
PRECOMP += -DDEBUG
CXX_COMPILE_FLAGS += -g -O0 -fstandalone-debug
else
CXX_COMPILE_FLAGS += -O3 -march=native
endif

LDFLAGS := $(addprefix -L,$(LIB_DIR)) -L${MKLROOT}/lib/intel64 -L${TBBROOT}lib/intel64_lin/gcc4.8 -Wl,--no-as-needed
ifeq ($(DEBUG),1)
LDFLAGS +=
endif
LIBS := -lnetcdf_c++4 -lnetcdf -lhdf5_hl -lhdf5 -lmkl_intel_lp64 -lmkl_tbb_thread -lmkl_core -ltbb -lstdc++ -lpthread -lm -ldl

#CXX_COMPILE_FLAGS += -I${NETCDF_C_DIR}
#CXX_COMPILE_FLAGS += -I${NDF5_DIR}
CXX_COMPILE_FLAGS += -I $(INC_DIR) -I /opt/llvm/llvm-10.0.0/llvm-10.0.0/include/c++/v1

SINGLE_PREC = 0
ifeq ($(SINGLE_PREC),1)
PRECOMP += -DSINGLE_PREC
endif

THREADING = multi
ifeq ($(THREADING),multi)
PRECOMP += -DBLAZE_USE_CPP_THREADS
endif

DEP_DIR := .d
$(shell mkdir -p $(DEP_DIR) >/dev/null)
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEP_DIR)/$*.Td

POSTCOMPILE = @mv -f $(DEP_DIR)/$*.Td $(DEP_DIR)/$*.d && touch $@

all: lib 
lib: $(CXX_LIB)
test_all: lib $(TEST_EXE)

#Define the compilers
CXXLINKER = ${CXX} ${CXXFLAGS}

###############################################################################
#                                                                             #
# Make rules for gamma_c                              												#
#                                                                             #
###############################################################################

#Define the C++ compile rules
%.o : %.cpp
$(BUILD_DIR)/%.o : $(SRC_DIR)/%.cpp $(DEP_DIR)/%.d
	@echo "Compiling $<"
	@${CXXLINKER} $(CXX_COMPILE_FLAGS) $(PRECOMP) $(DEPFLAGS) -c -o $@ $<
	@$(POSTCOMPILE)

% : %.cpp
$(BIN_DIR)/% : $(TEST_DIR)/%.cpp $(CXX_OBJ)
	@echo "Compiling and linking" $@
	@${CXXLINKER} $(CXX_COMPILE_FLAGS) $(LDFLAGS) -o $@ $< $(CXX_LIB) $(LIBS)
#	@${RM} PTSM3D_C.o


$(CXX_LIB): $(CXX_OBJ)
	@echo "Archiving $@"
	@${AR} crs $@ $^

#Empty target for the dependency files
$(DEP_DIR)/%.d: ;
.PRECIOUS: $(DEP_DIR)/%.d

include $(wildcard $(patsubst %,$(DEP_DIR)/%.d,$(basename $(SRCS))))

.PHONY: localclean

localclean:
	@rm -f $(CXX_OBJ) $(CXX_LIB) $(TEST_EXE)

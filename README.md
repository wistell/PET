# **PET is currently under active develepment and subject to change without notice**

# Plasma Equilibrium Toolkit
[Center for Plasma Theory and Computation](https://cptc.wisc.edu), University of Wisconsin-Madison

The Plasmas Equilibrium Toolkit (PET) library provides functionality for interfacing with different plasma equilibrium solvers for general three-dimensional magnetically confined plasma equilibria.  The library provides generalized coordinate representations and transformations for use in plasma simulation and analysis applications.

## Plasma MHD Equilibrium Interfaces
PET provides interfaces to the following MHD equilibrium solvers:
- VMEC (Variational Magnetic Equilibrium Calculation, part of [STELLOPT](https://github.com/PrincetonUniversity/STELLOPT))

## Coordinate representations
- PEST: $`\mathbf{B} = \nabla \psi \times \nabla \alpha`$, with
  - $`\psi`$: **toroidal** flux, $`\alpha = \theta -\iota(\psi) \zeta`$, and $`\zeta`$ is the independent field-line following coordinate with rotational transform $`\iota(\psi)`$
  - $`\psi`$: **poloidal** flux, $`\alpha = q(\psi)\theta -\zeta`$, and $`\theta`$ is the independent field-line foloowing coordinate with safety factor $`q(\psi) = 1/\iota(\psi)`$
- Boozer: $`\mathbf{B} = g(\psi) \nabla \phi_B + I(\psi) \nabla \theta_B + \nu(\psi,\theta_B,\phi_B)\nabla\psi`$ where $`g(\psi)`$ is the **poloidal** current, $`I(\psi)`$ is the **toroidal** current and $`\nu(\psi,\theta_B,\phi_B)`$ is a periodic function in $`(\theta_B,\phi_B)`$
- Cylindrical: $`\mathbf{B} = B^R \mathbf{e}_R + B^\phi \mathbf{e}_\phi + B^Z \mathbf{e}_Z`$
- 3D Cartesian: $`\mathbf{B} = B^X \mathbf{e}_X + B^Y \mathbf{e}_Y + B^Z \mathbf{e}_Z`$

## Contact
* * *
- Primary author: Benjamin Faber (bfaber at wisc dot edu)